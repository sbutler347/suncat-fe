from __future__ import unicode_literals

from django.apps import AppConfig


class CommonConfig(AppConfig):
    name = 'common_app'


class RelabelledConfig(AppConfig):
    name = 'common_app'
    verbose_name = 'Data Normalisation'
