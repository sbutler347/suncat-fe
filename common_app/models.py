from __future__ import unicode_literals

from django.db import models
from django.core.files import File
from datetime import datetime
# Create your models here.


class GlobalConfig(models.Model):
    gconf_id = models.AutoField(
        unique=True,
        primary_key=True
    )
    name = models.CharField(
        max_length=50,
        blank=True
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Global Config'
        managed = True
        db_table = 'global_config'


class MarcField(models.Model):
    mf_id = models.AutoField(
        unique=True,
        primary_key=True
    )
    marc_code = models.CharField(
        unique=True,
        max_length=6,
        blank=True
    )

    def __str__(self):
        return self.marc_code

    class Meta:
        ordering = ['marc_code']
        verbose_name = 'Marc Field'
        managed = True
        db_table = 'marc_field'


class MarcSubfield(models.Model):
    sf_id = models.AutoField(
        unique=True,
        primary_key=True
    )
    subfield_code = models.CharField(
        unique=True,
        max_length=1,
        blank=True
    )

    def __str__(self):
        return self.subfield_code

    class Meta:
        verbose_name = 'Marc Subfield'
        managed = True
        db_table = 'marc_subfield'


class InstitutionConfig(models.Model):
    FORMAT_CHOICES = (
        ('marc21', 'Marc 21'),
        ('aleph_seq', 'Aleph Sequential')
    )
    iconf_id = models.AutoField(
        unique=True,
        primary_key=True
    )
    name = models.CharField(
        max_length=50,
        blank=True
    )
    inst_code = models.CharField(
        unique=True,
        max_length=5,
        blank=True
    )
    ingest_file_format = models.CharField(
        choices=FORMAT_CHOICES,
        max_length=20
    )
    global_config = models.ForeignKey(
        GlobalConfig,
        on_delete=models.CASCADE
    )
    bib_mapping_field = models.CharField(
        max_length=3,
        blank=False
    )
    bib_mapping_subfield = models.CharField(
        max_length=1,
        blank=True
    )
    hol_mapping_field = models.CharField(
        max_length=3,
        blank=False
    )
    hol_mapping_subfield = models.CharField(
        max_length=1,
        blank=True
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Institution'
        managed = True
        db_table = 'institution_config'


class InstitutionLog(models.Model):
    logid = models.AutoField(
        unique=True,
        primary_key=True
    )
    logdate = models.DateTimeField()
    logfile = models.CharField(
        max_length=50,
        blank=True
    )

    def save(self, *args, **kwargs):
        f = open(
            '/home/lloptr347/Workspaces/suncatAdmin/logs/'+self.logfile, 'w')
        myFile = File(f)
        myFile.write(
            self.logfile+'\nlog creation datetime : '+str(datetime.now())+'\n')
        myFile.close
        super(InstitutionLog, self).save(*args, **kwargs)

    def __str__(self):
        return self.logfile

    class Meta:
        verbose_name = 'Log'
        managed = True
        db_table = 'institution_log'
