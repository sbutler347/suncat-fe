from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth.views import logout
from django.contrib.auth.views import login

admin.site.site_header = 'Suncat Normalisation Admin'

urlpatterns = [
    url(r'^', admin.site.urls),
    url(r'^admin/', admin.site.urls),
]
