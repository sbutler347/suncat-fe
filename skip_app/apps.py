from __future__ import unicode_literals

from django.apps import AppConfig


class SkipAppConfig(AppConfig):
    name = 'skip_app'
