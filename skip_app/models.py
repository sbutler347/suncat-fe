from __future__ import unicode_literals

from django.db import models
from common_app.models import InstitutionConfig
from common_app.models import MarcField
from common_app.models import MarcSubfield
# Create your models here.

class SkipGroup(models.Model):
    func_id = models.AutoField(
        unique=True,
        primary_key=True
    )
    group_name = models.IntegerField(
    )

    def __str__(self):
        return str(self.group_name)

    class Meta:
        verbose_name = 'Skip Group'
        managed = True
        db_table = 'skip_group'


class SkipFunction(models.Model):
    func_id = models.AutoField(
        unique=True,
        primary_key=True
    )
    display_name = models.CharField(
        max_length=50,
        blank=True
    )
    function_name = models.CharField(
        max_length=40,
        blank=True
    )

    def __str__(self):
        strOut = self.display_name
        return strOut

    class Meta:
        verbose_name = 'Skip Function'
        managed = True
        db_table = 'skip_functions'


class SkipConfig(models.Model):
    TYPE_CHOICES = (
        ('record', 'Record'),
        ('field', 'Field')
    )
    skipconf_id = models.AutoField(
        unique=True,
        primary_key=True
    )
    skip_function = models.ForeignKey(SkipFunction)
    skip_type = models.CharField(
        choices=TYPE_CHOICES,
        max_length=6
    )
    skip_marcfield = models.ForeignKey(
        MarcField,
        on_delete=models.CASCADE,
        related_name='skip_marcfield',
        blank=True,
        null=True
    )
    skip_group = models.ForeignKey(
        SkipGroup,
        on_delete=models.CASCADE,
        blank=True,
        null=True
    )
    condition_marcfield = models.ForeignKey(
        MarcField,
        on_delete=models.CASCADE,
        related_name='condition_marcfield',
        blank=True,
        null=True
    )
    skip_subfield_1 = models.ForeignKey(
        MarcSubfield,
        on_delete=models.CASCADE,
        related_name='skip_subfield',
        blank=True,
        null=True
    )
    ind_1 = models.CharField(
        max_length=1,
        blank=True
    )
    ind_2 = models.CharField(
        max_length=1,
        blank=True
    )
    skip_inst_conf = models.ForeignKey(
        InstitutionConfig,
        on_delete=models.CASCADE,
        blank=True,
        null=True
    )
    skip_param_1 = models.CharField(
        max_length=50,
        blank=True
    )

    def __str__(self):
        strOut = self.skip_function.display_name + \
            ' : '+self.skip_marcfield.marc_code
        return strOut

    class Meta:
        verbose_name = 'Skip configuration'
        managed = True
        db_table = 'skip_config'

