# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or
# field names.
from __future__ import unicode_literals

from django.db import models


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=80)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.BooleanField()
    username = models.CharField(unique=True, max_length=30)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.CharField(max_length=254)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey(
        'DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class GlobalConfig(models.Model):
    gconf_id = models.AutoField(unique=True, primary_key=True)
    name = models.CharField(max_length=30, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        managed = False
        db_table = 'global_config'


class Institution(models.Model):
    inst_id = models.AutoField(unique=True, primary_key=True)
    name = models.CharField(max_length=40, blank=True)
    inst_code = models.CharField(max_length=5, blank=True)

    def __str__(self):
        strOut = self.name+' : '+self.inst_code
        return strOut

    class Meta:
        managed = False
        db_table = 'institution'


class InstitutionConfig(models.Model):
    iconf_id = models.AutoField(unique=True, primary_key=True)
    inst = models.ForeignKey(Institution, blank=True, null=True)
#    normalisation_conf_id = models.IntegerField(blank=True, null=True)
    global_config = models.ForeignKey(GlobalConfig, blank=True, null=True)

    def __str__(self):
        strOut = self.inst.name+' : '+self.global_config.name
        return self.inst.name

    class Meta:
        managed = False
        db_table = 'institution_config'


class InstitutionLog(models.Model):
    logid = models.AutoField(unique=True, primary_key=True)
    inst_conf = models.ForeignKey(InstitutionConfig, blank=True, null=True)
    logdate = models.DateTimeField(blank=True, null=True)
    logfile = models.CharField(max_length=50, blank=True)

    def __str__(self):
        strOut = self.logdate+' : '+self.logfile+' : '+self.inst_conf
        return strOut

    class Meta:
        managed = False
        db_table = 'institution_log'


class MarcField(models.Model):
    mf_id = models.AutoField(unique=True, primary_key=True)
    marc_code = models.CharField(max_length=3, blank=True)

    def __str__(self):
        return self.marc_code

    class Meta:
        managed = False
        db_table = 'marc_field'


class MarcSubfield(models.Model):
    sf_id = models.AutoField(unique=True, primary_key=True)
    subfield_code = models.CharField(max_length=1, blank=True)

    def __str__(self):
        return self.subfield_code

    class Meta:
        managed = False
        db_table = 'marc_subfield'


class NormalisationFunctions(models.Model):
    func_id = models.AutoField(unique=True, primary_key=True)
    name = models.CharField(max_length=40, blank=True)
    sequence = models.IntegerField()

    def __str__(self):
        return self.name

    class Meta:
        managed = False
        db_table = 'normalisation_functions'


class NormalisationConfig(models.Model):
    nconf_id = models.AutoField(unique=True, primary_key=True)
    inst_conf = models.ForeignKey(InstitutionConfig, blank=True, null=True)
    #function_id = models.ManyToManyField(NormalisationFunctions)
    marcfield = models.ForeignKey(MarcField, blank=True, null=True)
    param_1 = models.CharField(max_length=20, blank=True)
    param_2 = models.CharField(max_length=20, blank=True)

    def __str__(self):
        strOut = self.inst_conf.inst.name+' : '+self.marcfield.marc_code
        return strOut

    class Meta:
        managed = False
        db_table = 'normalisation_config'


# class NormalisationFunctionMap(models.Model):
#     mapkey = models.AutoField(unique=True, primary_key=True)
#     function = models.ForeignKey('NormalisationFunctions', blank=True, null=True)
#     normconf = models.ForeignKey(NormalisationConfig, blank=True, null=True)
#     class Meta:
#         managed = False
#         db_table = 'normalisation_function_map'

# class NormalisationSubfield2Map(models.Model):
#     map_id = models.AutoField(unique=True, primary_key=True)
#     norm_conf = models.ForeignKey(NormalisationConfig, blank=True, null=True)
#     subfield = models.ForeignKey(MarcSubfield, blank=True, null=True)
#     class Meta:
#         managed = False
#         db_table = 'normalisation_subfield2_map'

# class NormalisationSubfieldMap(models.Model):
#     map_id = models.AutoField(unique=True, primary_key=True)
#     norm_conf = models.ForeignKey(NormalisationConfig, blank=True, null=True)
#     subfield = models.ForeignKey(MarcSubfield, blank=True, null=True)
#     class Meta:
#         managed = False
#         db_table = 'normalisation_subfield_map'
