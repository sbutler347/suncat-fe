from django.test import TestCase
from suncat.models import MarcField
    
class test_marc_field(TestCase):
	
	def setUp(self):
		MarcField.objects.create(marc_code="088")

	def testInserted(self):
		t1 = MarcField.objects.get(marc_code="088")

		self.assertEqual(t1.marc_code, "088")

	def testEditFieldConfig(self):
		t1 = MarcField.objects.get(marc_code="088")
		setattr(t1, 'marc_code', '089')
		t1.save()
		t2 = MarcField.objects.get(marc_code="089")
		
		self.assertEqual(t2.marc_code, "089")

