from django.test import TestCase
from suncat.models import InstitutionConfig
from suncat.models import InstitutionLog
from suncat.models import GlobalConfig
from suncat.models import Institution

class test_institution_config(TestCase):
	
	def setUp(self):
		inst = Institution.objects.create(name="Southampton", inst_code="SOT01")
		gconf = GlobalConfig.objects.create(name="TestName 1")
		log = InstitutionLog.objects.create(logdate='2016-01-18 14:52+00', logfile="test.log")
		InstitutionConfig.objects.create(inst=inst, log=log, global_config = gconf)

	def testInsert(self):
		i1 = Institution.objects.get(name="Southampton")
		t1 = InstitutionConfig.objects.get(inst=i1.inst_id)
		
		self.assertEqual(t1.inst.name, "Southampton")

	def testEdit(self):
		i1 = Institution.objects.get(inst_code="SOT01")
		i2 = Institution.objects.create(name="Suncat", inst_code="SCT02")
		t1 = InstitutionConfig.objects.get(inst=i1)
		setattr(t1, 'inst', i2)
		t1.save()
		t2 = InstitutionConfig.objects.get(inst=i2)
		
		self.assertEqual(t2.inst, i2)

