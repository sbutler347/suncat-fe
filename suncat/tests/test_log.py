from django.test import TestCase
from suncat.models import InstitutionLog
import datetime

class test_log(TestCase):
	
	def setUp(self):
		InstitutionLog.objects.create(logdate='2016-01-18 14:52+00', logfile="test.log")

	def testInsert(self):
		t1 = InstitutionLog.objects.get(logfile="test.log")
		
		self.assertEqual(str(t1.logdate), "2016-01-18 14:52:00+00:00")

	def testEditLog(self):
		t1 = InstitutionLog.objects.get(logfile="test.log")
		setattr(t1, 'logfile', 'test2.log')
		t1.save()
		t2 = InstitutionLog.objects.get(logfile="test2.log")
		
		self.assertEqual(t2.logfile, "test2.log")

