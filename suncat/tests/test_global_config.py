from django.test import TestCase
from suncat.models import GlobalConfig
    
class test_global_config(TestCase):
	
	def setUp(self):
		GlobalConfig.objects.create(name="TestName 1")

	def testInserted(self):
		t1 = GlobalConfig.objects.get(name="TestName 1")

		self.assertEqual(t1.name, "TestName 1")

	def testEditGlobalConfig(self):
		t1 = GlobalConfig.objects.get(name="TestName 1")
		setattr(t1, 'name', 'TestName 2')
		t1.save()
		t2 = GlobalConfig.objects.get(name="TestName 2")
		
		self.assertEqual(t2.name, "TestName 2")

