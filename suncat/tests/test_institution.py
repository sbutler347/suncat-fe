from django.test import TestCase
from suncat.models import Institution
    
class test_institution(TestCase):
	
	def setUp(self):
		Institution.objects.create(name="Southampton", inst_code="SOT01")

	def testInsert(self):
		t1 = Institution.objects.get(inst_code="SOT01")

		self.assertEqual(t1.name, "Southampton")
		self.assertEqual(t1.inst_code, "SOT01")

	def testEdit(self):
		t1 = Institution.objects.get(inst_code="SOT01")
		setattr(t1, 'inst_code', 'SOT02')
		t1.save()
		t2 = Institution.objects.get(name="Southampton")
		
		self.assertEqual(t2.inst_code, "SOT02")

