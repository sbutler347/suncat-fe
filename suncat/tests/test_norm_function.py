from django.test import TestCase
from suncat.models import NormFunction
    
class test_norm_function(TestCase):
	
	def setUp(self):
		NormFunction.objects.create(displayName="disp1", functionName="func1")

	def testInserted(self):
		t1 = NormFunction.objects.get(displayName="disp1")

		self.assertEqual(t1.functionName, "func1")

	def testEditFunctionConfig(self):
		t1 = NormFunction.objects.get(displayName="disp1")
		setattr(t1, 'functionName', 'func2')
		t1.save()
		t2 = NormFunction.objects.get(displayName="disp1")
		
		self.assertEqual(t2.functionName, "func2")

