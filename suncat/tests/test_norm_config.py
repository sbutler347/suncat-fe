from django.test import TestCase
from suncat.models import NormFunctionConfig
from suncat.models import NormFunctionConfig
from suncat.models import NormFunction
from suncat.models import FunctionMap
from suncat.models import Institution
from suncat.models import GlobalConfig
from suncat.models import InstitutionLog
from suncat.models import InstitutionConfig
from suncat.models import MarcField
from suncat.models import MarcSubfield
from suncat.models import NormalisationConfig

class test_norm_config(TestCase):
	
	def setUp(self):
		fcConfig = NormFunctionConfig.objects.create(name="fcName1", sequence=1, param_1="a", param_2="z")
		nfunc = NormFunction.objects.create(displayName="disp1", functionName="func1")
		functionMapObj = FunctionMap.objects.create(name = "fMapName1", function = nfunc, function_config = fcConfig)
		inst = Institution.objects.create(name="Southampton", inst_code="SOT01")
		gconf = GlobalConfig.objects.create(name="TestName 1")
		log = InstitutionLog.objects.create(logdate='2016-01-18 14:52+00', logfile="test.log")
		iConf = InstitutionConfig.objects.create(inst=inst, log=log, global_config = gconf)
		mField = MarcField.objects.create(marc_code="088")
		mSubField1 = MarcSubfield.objects.create(subfield_code="a")
		mSubField2 = MarcSubfield.objects.create(subfield_code="b")

		functionMapObj.save()
		nc1 =NormalisationConfig.objects.create(inst_conf = iConf, marcfield = mField, subfield_1 = mSubField1, subfield_2 = mSubField2 )
		nc1.function_map.add(functionMapObj)

	def testInserted(self):
		n1 = FunctionMap.objects.get(name="fMapName1")
		t1 = NormalisationConfig.objects.get(function_map=n1.fm_id)

		self.assertEqual(t1.inst_conf.inst.name, "Southampton")

	def testEditNormConfig(self):
		i1 = Institution.objects.get(name="Southampton")
		n1 = InstitutionConfig.objects.get(inst=i1.inst_id)
		t1 = NormalisationConfig.objects.get(inst_conf = n1)
		mcField = MarcField.objects.create(marc_code="090")	
		setattr(t1, 'marcfield', mcField)
		t1.save()
		t2 = MarcField.objects.get(marc_code="090")
		
		self.assertEqual(t2.marc_code, "090")
