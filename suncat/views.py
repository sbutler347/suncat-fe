from django.http import Http404
from django.contrib.auth import authenticate
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from django.contrib import messages

from .models import *
from .forms import *


def index(request):
    return render(request, 'suncat/index.html')


@login_required
def gconfIndex(request):
    gconfig_list = GlobalConfig.objects.order_by('name')[:5]
    context = {'gconfig_list': gconfig_list}
    return render(request, 'suncat/gindex.html', context)


@login_required
def iconfIndex(request):
    iconfig_list = InstitutionConfig.objects.order_by('inst')[:5]
    context = {'iconfig_list': iconfig_list}
    return render(request, 'suncat/iindex.html', context)


@login_required
def nconfIndex(request):
    nconfig_list = NormalisationConfig.objects.order_by('inst_conf_id')[:5]
    context = {'nconfig_list': nconfig_list}
    return render(request, 'suncat/nindex.html', context)


@login_required
def gConfEdit(request, gconf_id):
    try:
        globalConf = GlobalConfig.objects.get(pk=gconf_id)
        form = GlobalConfigForm(instance=globalConf)
    except GlobalConfig.DoesNotExist:
        raise Http404("Config does not exist")
    return render(request, 'suncat/globalConfInput.html', {'form': form})


@login_required
def iConfEdit(request, iconf_id):
    try:
        instConf = InstitutionConfig.objects.get(pk=iconf_id)
        form = InstitutionConfigForm(instance=instConf)
    except InstitutionConfig.DoesNotExist:
        raise Http404("Institution Config does not exist")
    return render(request, 'suncat/instConfInput.html', {'form': form})


@login_required
def nConfEdit(request, nconf_id):
    try:
        normConf = NormalisationConfig.objects.get(pk=nconf_id)
        form = NormalisationConfigForm(instance=normConf)
    except InstitutionConfig.DoesNotExist:
        raise Http404("Institution Config does not exist")
    return render(request, 'suncat/normConfInput.html', {'form': form})
