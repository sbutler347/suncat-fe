# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Remove `managed = False` lines for those models you wish
#     to give write DB access
# Feel free to rename the models, but don't rename db_table
#     values or field names.
#
# Also note: You'll have to insert the output of
# 'django-admin.py sqlcustom [appname]'
# into your database.
from __future__ import unicode_literals
from django.db import models
from common_app.models import InstitutionConfig
from common_app.models import MarcField
from common_app.models import MarcSubfield


class NormalisationGroup(models.Model):
    func_id = models.AutoField(
        unique=True,
        primary_key=True
    )
    group_name = models.IntegerField(
        # max_length=50,
        # blank=True
    )

    def __str__(self):
        return str(self.group_name)

    class Meta:
        verbose_name = 'Skip Group'
        managed = True
        db_table = 'normalisation_group'


class NormFunction(models.Model):
    func_id = models.AutoField(
        unique=True,
        primary_key=True
    )
    display_name = models.CharField(
        unique=True,
        max_length=50,
        blank=True
    )
    function_name = models.CharField(
        unique=True,
        max_length=40,
        blank=True
    )

    def __str__(self):
        return str(self.display_name)

    class Meta:
        verbose_name = 'Normalisation Function'
        managed = True
        db_table = 'normalisation_functions'


class NormalisationConfig(models.Model):
    SEQ_CHOICES = (
        ('1', '1'),
        ('2', '2'),
        ('3', '3'),
        ('4', '4'),
        ('5', '5'),
        ('6', '6'),
        ('7', '7'),
        ('8', '8'),
        ('9', '9'),
        ('10', '10'),
        ('11', '11'),
        ('12', '12'),
        ('13', '13'),
        ('14', '14'),
        ('15', '15'),
        ('16', '16'),
        ('17', '17'),
        ('18', '18'),
        ('19', '19'),
        ('20', '20'),
    )
    nconf_id = models.AutoField(unique=True, primary_key=True)
    function = models.ForeignKey(NormFunction)
    transition_step = models.BooleanField()
    marcfield = models.ForeignKey(
        MarcField,
        on_delete=models.CASCADE,
        blank=True,
        null=True
    )

    inst_conf = models.ForeignKey(
        InstitutionConfig,
        on_delete=models.CASCADE,
        blank=True,
        null=True
    )
    subfield_1 = models.ForeignKey(
        MarcSubfield,
        on_delete=models.CASCADE,
        related_name='existing_subfield',
        blank=True,
        null=True
    )
    subfield_2 = models.ForeignKey(
        MarcSubfield,
        on_delete=models.CASCADE,
        related_name='new_subfield',
        blank=True,
        null=True
    )
    group_sequence = models.ForeignKey(
        NormalisationGroup,
        on_delete=models.CASCADE,
        blank=True,
        null=True
    )
    function_sequence = models.CharField(
        choices=SEQ_CHOICES,
        max_length=10
    )
    ind_1 = models.CharField(
        max_length=1,
        blank=True
    )
    ind_2 = models.CharField(
        max_length=1,
        blank=True

    )
    param_1 = models.CharField(
        max_length=50,
        blank=True
    )
    param_2 = models.CharField(
        max_length=50,
        blank=True
    )

    def __str__(self):
        strOut = self.function.display_name+' : '+self.marcfield.marc_code
        return strOut

    class Meta:
        verbose_name = 'Normalisation configuration'
        managed = True
        db_table = 'normalisation_config'


class HoldingsPreNormConfig(models.Model):
    SEQ_CHOICES = (
        ('1', '1'),
        ('2', '2'),
        ('3', '3'),
        ('4', '4'),
        ('5', '5'),
        ('6', '6'),
        ('7', '7'),
        ('8', '8'),
        ('9', '9'),
        ('10', '10'),
        ('11', '11'),
        ('12', '12'),
        ('13', '13'),
        ('14', '14'),
        ('15', '15'),
        ('16', '16'),
        ('17', '17'),
        ('18', '18'),
        ('19', '19'),
        ('20', '20'),
    )
    nconf_id = models.AutoField(unique=True, primary_key=True)
    function = models.ForeignKey(NormFunction)
    transition_step = models.BooleanField()
    marcfield = models.ForeignKey(
        MarcField,
        on_delete=models.CASCADE,
        blank=True,
        null=True
    )

    inst_conf = models.ForeignKey(
        InstitutionConfig,
        on_delete=models.CASCADE,
        blank=True,
        null=True
    )
    subfield_1 = models.ForeignKey(
        MarcSubfield,
        on_delete=models.CASCADE,
        related_name='existing_hol_subfield',
        blank=True,
        null=True
    )
    subfield_2 = models.ForeignKey(
        MarcSubfield,
        on_delete=models.CASCADE,
        related_name='new_hol_subfield',
        blank=True,
        null=True
    )
    group_sequence = models.ForeignKey(
        NormalisationGroup,
        on_delete=models.CASCADE,
        blank=True,
        null=True
    )
    function_sequence = models.CharField(
        choices=SEQ_CHOICES,
        max_length=10
    )
    ind_1 = models.CharField(
        max_length=1,
        blank=True
    )
    ind_2 = models.CharField(
        max_length=1,
        blank=True

    )
    param_1 = models.CharField(
        max_length=50,
        blank=True
    )
    param_2 = models.CharField(
        max_length=50,
        blank=True
    )

    def __str__(self):
        strOut = self.function.display_name+' : '+self.marcfield.marc_code
        return strOut

    class Meta:
        verbose_name = 'Holdings pre-normalisation configuration'
        managed = True
        db_table = 'holdings_normalisation_config'
