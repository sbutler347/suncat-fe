from django import forms
from django.forms import ModelForm
from suncat.models import *
from .widgets import AddAnotherWidgetWrapper


class GlobalConfigForm(ModelForm):

    class Meta:
        model = GlobalConfig
        fields = ['gconf_id', 'name']


class InstitutionForm(ModelForm):

    class Meta:
        model = Institution
        fields = ['inst_id', 'name', 'inst_code']

# class InstitutionConfigForm(ModelForm):
#      class Meta:
#          model = InstitutionConfig
#          fields = ['iconf_id', 'global_config', 'inst', 'log']
#          widget = AddAnotherWidgetWrapper(forms.Select(), 'global_config')


class InstitutionConfigForm(ModelForm):

    class Meta:
        model = InstitutionConfig
        fields = ['iconf_id', 'global_config']
        fields = ['inst', 'log']


class InstitutionLogForm(ModelForm):

    class Meta:
        model = InstitutionLog
        fields = ['logid', 'logdate', 'logfile']


class MarcFieldForm(ModelForm):

    class Meta:
        model = MarcField
        fields = ['mf_id', 'marc_code']


class MarcSubfieldForm(ModelForm):

    class Meta:
        model = MarcSubfield
        fields = ['sf_id', 'subfield_code']


class NormalisationConfigForm(ModelForm):

    class Meta:
        model = NormalisationConfig
        fields = ['nconf_id', 'inst_conf',
                  'marcfield', 'subfield_1', 'subfield_2']
