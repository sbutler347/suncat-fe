from django.contrib import admin

from common_app.models import GlobalConfig
from common_app.models import InstitutionConfig
from common_app.models import InstitutionLog
from common_app.models import MarcField
from common_app.models import MarcSubfield
from .models import NormalisationConfig
from .models import HoldingsPreNormConfig
from .models import NormFunction
from .models import NormalisationGroup
from skip_app.models import SkipConfig
from skip_app.models import SkipFunction
from skip_app.models import SkipGroup


@admin.register(GlobalConfig)
class GlobalConfigAdmin(admin.ModelAdmin):

    def get_model_perms(self, request):
        return {}


@admin.register(MarcField)
class MarcFieldAdmin(admin.ModelAdmin):

    def get_model_perms(self, request):
        return {}


@admin.register(MarcSubfield)
class MarcSubfieldAdmin(admin.ModelAdmin):

    def get_model_perms(self, request):
        return {}


@admin.register(NormalisationConfig)
class NormalisationConfigAdmin(admin.ModelAdmin):

    def get_model_perms(self, request):
        return {}


@admin.register(HoldingsPreNormConfig)
class HoldingsPreNormConfigAdmin(admin.ModelAdmin):

    def get_model_perms(self, request):
        return {}


@admin.register(NormalisationGroup)
class NormalisationGroupAdmin(admin.ModelAdmin):

    def get_model_perms(self, request):
        return {}


@admin.register(SkipConfig)
class SkipConfigAdmin(admin.ModelAdmin):

    def get_model_perms(self, request):
        return {}


@admin.register(SkipGroup)
class SkipGroupAdmin(admin.ModelAdmin):

    def get_model_perms(self, request):
        return {}


class NormalisationEntryInline(admin.TabularInline):
    model = NormalisationConfig
    extra = 1


class HoldingsPreNormConfigInline(admin.TabularInline):
    model = HoldingsPreNormConfig
    extra = 1


class SkipFunctionEntryInline(admin.TabularInline):
    model = SkipConfig
    extra = 1


@admin.register(InstitutionLog)
class InstitutionLogAdmin(admin.ModelAdmin):
    search_fields = ['logfile']


@admin.register(InstitutionConfig)
class InstitutionConfigAdmin(admin.ModelAdmin):
    search_fields = ['global_config__name', 'name', 'inst_code']
    list_filter = ['name']
    search_fields = ['name']
    inlines = [
        SkipFunctionEntryInline,
        HoldingsPreNormConfigInline,
        NormalisationEntryInline
    ]


@admin.register(SkipFunction)
class SkipFunctionAdmin(admin.ModelAdmin):

    def get_model_perms(self, request):
        return {}


@admin.register(NormFunction)
class NormFunctionAdmin(admin.ModelAdmin):

    def get_model_perms(self, request):
        return {}
