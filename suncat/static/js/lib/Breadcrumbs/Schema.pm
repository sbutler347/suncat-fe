use utf8;
package Breadcrumbs::Schema;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Schema';

__PACKAGE__->load_namespaces;


# Created by DBIx::Class::Schema::Loader v0.07039 @ 2016-01-19 13:42:06
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:PMwf8MG5stPqiDv9j0zqRw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
