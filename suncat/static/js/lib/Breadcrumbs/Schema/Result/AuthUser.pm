use utf8;
package Breadcrumbs::Schema::Result::AuthUser;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Breadcrumbs::Schema::Result::AuthUser

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<auth_user>

=cut

__PACKAGE__->table("auth_user");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'auth_user_id_seq'

=head2 password

  data_type: 'varchar'
  is_nullable: 0
  size: 128

=head2 last_login

  data_type: 'timestamp with time zone'
  is_nullable: 1

=head2 is_superuser

  data_type: 'boolean'
  is_nullable: 0

=head2 username

  data_type: 'varchar'
  is_nullable: 0
  size: 30

=head2 first_name

  data_type: 'varchar'
  is_nullable: 0
  size: 30

=head2 last_name

  data_type: 'varchar'
  is_nullable: 0
  size: 30

=head2 email

  data_type: 'varchar'
  is_nullable: 0
  size: 254

=head2 is_staff

  data_type: 'boolean'
  is_nullable: 0

=head2 is_active

  data_type: 'boolean'
  is_nullable: 0

=head2 date_joined

  data_type: 'timestamp with time zone'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "auth_user_id_seq",
  },
  "password",
  { data_type => "varchar", is_nullable => 0, size => 128 },
  "last_login",
  { data_type => "timestamp with time zone", is_nullable => 1 },
  "is_superuser",
  { data_type => "boolean", is_nullable => 0 },
  "username",
  { data_type => "varchar", is_nullable => 0, size => 30 },
  "first_name",
  { data_type => "varchar", is_nullable => 0, size => 30 },
  "last_name",
  { data_type => "varchar", is_nullable => 0, size => 30 },
  "email",
  { data_type => "varchar", is_nullable => 0, size => 254 },
  "is_staff",
  { data_type => "boolean", is_nullable => 0 },
  "is_active",
  { data_type => "boolean", is_nullable => 0 },
  "date_joined",
  { data_type => "timestamp with time zone", is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<auth_user_username_key>

=over 4

=item * L</username>

=back

=cut

__PACKAGE__->add_unique_constraint("auth_user_username_key", ["username"]);

=head1 RELATIONS

=head2 auth_user_groups

Type: has_many

Related object: L<Breadcrumbs::Schema::Result::AuthUserGroup>

=cut

__PACKAGE__->has_many(
  "auth_user_groups",
  "Breadcrumbs::Schema::Result::AuthUserGroup",
  { "foreign.user_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 auth_user_user_permissions

Type: has_many

Related object: L<Breadcrumbs::Schema::Result::AuthUserUserPermission>

=cut

__PACKAGE__->has_many(
  "auth_user_user_permissions",
  "Breadcrumbs::Schema::Result::AuthUserUserPermission",
  { "foreign.user_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 django_admin_logs

Type: has_many

Related object: L<Breadcrumbs::Schema::Result::DjangoAdminLog>

=cut

__PACKAGE__->has_many(
  "django_admin_logs",
  "Breadcrumbs::Schema::Result::DjangoAdminLog",
  { "foreign.user_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07039 @ 2016-01-19 13:42:06
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:ynVnDih/IYiM1kW3TMBy2A


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
