use utf8;
package Breadcrumbs::Schema::Result::DjangoAdminLog;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Breadcrumbs::Schema::Result::DjangoAdminLog

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<django_admin_log>

=cut

__PACKAGE__->table("django_admin_log");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'django_admin_log_id_seq'

=head2 action_time

  data_type: 'timestamp with time zone'
  is_nullable: 0

=head2 object_id

  data_type: 'text'
  is_nullable: 1

=head2 object_repr

  data_type: 'varchar'
  is_nullable: 0
  size: 200

=head2 action_flag

  data_type: 'smallint'
  is_nullable: 0

=head2 change_message

  data_type: 'text'
  is_nullable: 0

=head2 content_type_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 user_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "django_admin_log_id_seq",
  },
  "action_time",
  { data_type => "timestamp with time zone", is_nullable => 0 },
  "object_id",
  { data_type => "text", is_nullable => 1 },
  "object_repr",
  { data_type => "varchar", is_nullable => 0, size => 200 },
  "action_flag",
  { data_type => "smallint", is_nullable => 0 },
  "change_message",
  { data_type => "text", is_nullable => 0 },
  "content_type_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "user_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 content_type

Type: belongs_to

Related object: L<Breadcrumbs::Schema::Result::DjangoContentType>

=cut

__PACKAGE__->belongs_to(
  "content_type",
  "Breadcrumbs::Schema::Result::DjangoContentType",
  { id => "content_type_id" },
  {
    is_deferrable => 1,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "NO ACTION",
  },
);

=head2 user

Type: belongs_to

Related object: L<Breadcrumbs::Schema::Result::AuthUser>

=cut

__PACKAGE__->belongs_to(
  "user",
  "Breadcrumbs::Schema::Result::AuthUser",
  { id => "user_id" },
  { is_deferrable => 1, on_delete => "NO ACTION", on_update => "NO ACTION" },
);


# Created by DBIx::Class::Schema::Loader v0.07039 @ 2016-01-19 13:42:06
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:Ob01NOvaWIRofAO5tO17pA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
