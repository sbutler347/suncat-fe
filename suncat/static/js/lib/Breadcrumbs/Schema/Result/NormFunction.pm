use utf8;
package Breadcrumbs::Schema::Result::NormFunction;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Breadcrumbs::Schema::Result::NormFunction

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<norm_functions>

=cut

__PACKAGE__->table("norm_functions");

=head1 ACCESSORS

=head2 func_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'norm_functions_func_id_seq'

=head2 displayname

  data_type: 'varchar'
  is_nullable: 0
  size: 40

=head2 functionname

  data_type: 'varchar'
  is_nullable: 0
  size: 40

=cut

__PACKAGE__->add_columns(
  "func_id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "norm_functions_func_id_seq",
  },
  "displayname",
  { data_type => "varchar", is_nullable => 0, size => 40 },
  "functionname",
  { data_type => "varchar", is_nullable => 0, size => 40 },
);

=head1 PRIMARY KEY

=over 4

=item * L</func_id>

=back

=cut

__PACKAGE__->set_primary_key("func_id");

=head1 RELATIONS

=head2 function_maps

Type: has_many

Related object: L<Breadcrumbs::Schema::Result::FunctionMap>

=cut

__PACKAGE__->has_many(
  "function_maps",
  "Breadcrumbs::Schema::Result::FunctionMap",
  { "foreign.function_id" => "self.func_id" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07039 @ 2016-01-19 13:42:06
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:7+bxWWBmDomw8Q9znJo8tA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
