use utf8;
package Breadcrumbs::Schema::Result::FunctionMap;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Breadcrumbs::Schema::Result::FunctionMap

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<function_map>

=cut

__PACKAGE__->table("function_map");

=head1 ACCESSORS

=head2 fm_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'function_map_fm_id_seq'

=head2 name

  data_type: 'varchar'
  is_nullable: 0
  size: 50

=head2 function_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 function_config_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "fm_id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "function_map_fm_id_seq",
  },
  "name",
  { data_type => "varchar", is_nullable => 0, size => 50 },
  "function_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "function_config_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</fm_id>

=back

=cut

__PACKAGE__->set_primary_key("fm_id");

=head1 RELATIONS

=head2 function

Type: belongs_to

Related object: L<Breadcrumbs::Schema::Result::NormFunction>

=cut

__PACKAGE__->belongs_to(
  "function",
  "Breadcrumbs::Schema::Result::NormFunction",
  { func_id => "function_id" },
  { is_deferrable => 1, on_delete => "NO ACTION", on_update => "NO ACTION" },
);

=head2 function_config

Type: belongs_to

Related object: L<Breadcrumbs::Schema::Result::NormFunctionConfig>

=cut

__PACKAGE__->belongs_to(
  "function_config",
  "Breadcrumbs::Schema::Result::NormFunctionConfig",
  { nfconf_id => "function_config_id" },
  { is_deferrable => 1, on_delete => "NO ACTION", on_update => "NO ACTION" },
);

=head2 normalisation_config_function_maps

Type: has_many

Related object: L<Breadcrumbs::Schema::Result::NormalisationConfigFunctionMap>

=cut

__PACKAGE__->has_many(
  "normalisation_config_function_maps",
  "Breadcrumbs::Schema::Result::NormalisationConfigFunctionMap",
  { "foreign.functionmap_id" => "self.fm_id" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07039 @ 2016-01-19 13:42:06
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:XamKkWCOn+Zl8McfWBDl3A


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
