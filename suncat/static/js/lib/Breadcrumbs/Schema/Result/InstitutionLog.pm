use utf8;
package Breadcrumbs::Schema::Result::InstitutionLog;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Breadcrumbs::Schema::Result::InstitutionLog

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<institution_log>

=cut

__PACKAGE__->table("institution_log");

=head1 ACCESSORS

=head2 logid

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'institution_log_logid_seq'

=head2 logdate

  data_type: 'timestamp with time zone'
  is_nullable: 0

=head2 logfile

  data_type: 'varchar'
  is_nullable: 0
  size: 50

=cut

__PACKAGE__->add_columns(
  "logid",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "institution_log_logid_seq",
  },
  "logdate",
  { data_type => "timestamp with time zone", is_nullable => 0 },
  "logfile",
  { data_type => "varchar", is_nullable => 0, size => 50 },
);

=head1 PRIMARY KEY

=over 4

=item * L</logid>

=back

=cut

__PACKAGE__->set_primary_key("logid");

=head1 RELATIONS

=head2 institution_config

Type: might_have

Related object: L<Breadcrumbs::Schema::Result::InstitutionConfig>

=cut

__PACKAGE__->might_have(
  "institution_config",
  "Breadcrumbs::Schema::Result::InstitutionConfig",
  { "foreign.log_id" => "self.logid" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07039 @ 2016-01-19 13:42:06
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:+/EZclloHIdoFZ/S1XqJww


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
