use utf8;
package Breadcrumbs::Schema::Result::AuthGroup;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Breadcrumbs::Schema::Result::AuthGroup

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<auth_group>

=cut

__PACKAGE__->table("auth_group");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'auth_group_id_seq'

=head2 name

  data_type: 'varchar'
  is_nullable: 0
  size: 80

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "auth_group_id_seq",
  },
  "name",
  { data_type => "varchar", is_nullable => 0, size => 80 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<auth_group_name_key>

=over 4

=item * L</name>

=back

=cut

__PACKAGE__->add_unique_constraint("auth_group_name_key", ["name"]);

=head1 RELATIONS

=head2 auth_group_permissions

Type: has_many

Related object: L<Breadcrumbs::Schema::Result::AuthGroupPermission>

=cut

__PACKAGE__->has_many(
  "auth_group_permissions",
  "Breadcrumbs::Schema::Result::AuthGroupPermission",
  { "foreign.group_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 auth_user_groups

Type: has_many

Related object: L<Breadcrumbs::Schema::Result::AuthUserGroup>

=cut

__PACKAGE__->has_many(
  "auth_user_groups",
  "Breadcrumbs::Schema::Result::AuthUserGroup",
  { "foreign.group_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07039 @ 2016-01-19 13:42:06
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:3ZwyQMNVEJ9D5z83SCdXNA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
