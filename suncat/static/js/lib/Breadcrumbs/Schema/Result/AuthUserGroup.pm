use utf8;
package Breadcrumbs::Schema::Result::AuthUserGroup;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Breadcrumbs::Schema::Result::AuthUserGroup

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<auth_user_groups>

=cut

__PACKAGE__->table("auth_user_groups");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'auth_user_groups_id_seq'

=head2 user_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 group_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "auth_user_groups_id_seq",
  },
  "user_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "group_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<auth_user_groups_user_id_94350c0c_uniq>

=over 4

=item * L</user_id>

=item * L</group_id>

=back

=cut

__PACKAGE__->add_unique_constraint(
  "auth_user_groups_user_id_94350c0c_uniq",
  ["user_id", "group_id"],
);

=head1 RELATIONS

=head2 group

Type: belongs_to

Related object: L<Breadcrumbs::Schema::Result::AuthGroup>

=cut

__PACKAGE__->belongs_to(
  "group",
  "Breadcrumbs::Schema::Result::AuthGroup",
  { id => "group_id" },
  { is_deferrable => 1, on_delete => "NO ACTION", on_update => "NO ACTION" },
);

=head2 user

Type: belongs_to

Related object: L<Breadcrumbs::Schema::Result::AuthUser>

=cut

__PACKAGE__->belongs_to(
  "user",
  "Breadcrumbs::Schema::Result::AuthUser",
  { id => "user_id" },
  { is_deferrable => 1, on_delete => "NO ACTION", on_update => "NO ACTION" },
);


# Created by DBIx::Class::Schema::Loader v0.07039 @ 2016-01-19 13:42:06
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:zK8Xp6XpJqEnxkw3vXTY7w


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
