use utf8;
package Breadcrumbs::Schema::Result::InstitutionConfig;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Breadcrumbs::Schema::Result::InstitutionConfig

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<institution_config>

=cut

__PACKAGE__->table("institution_config");

=head1 ACCESSORS

=head2 iconf_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'institution_config_iconf_id_seq'

=head2 global_config_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 inst_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 log_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 name

  data_type: 'varchar'
  is_nullable: 0
  size: 40

=cut

__PACKAGE__->add_columns(
  "iconf_id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "institution_config_iconf_id_seq",
  },
  "global_config_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "inst_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "log_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "name",
  { data_type => "varchar", is_nullable => 0, size => 40 },
);

=head1 PRIMARY KEY

=over 4

=item * L</iconf_id>

=back

=cut

__PACKAGE__->set_primary_key("iconf_id");

=head1 UNIQUE CONSTRAINTS

=head2 C<institution_config_inst_id_b8141704_uniq>

=over 4

=item * L</inst_id>

=back

=cut

__PACKAGE__->add_unique_constraint("institution_config_inst_id_b8141704_uniq", ["inst_id"]);

=head2 C<institution_config_log_id_key>

=over 4

=item * L</log_id>

=back

=cut

__PACKAGE__->add_unique_constraint("institution_config_log_id_key", ["log_id"]);

=head1 RELATIONS

=head2 global_config

Type: belongs_to

Related object: L<Breadcrumbs::Schema::Result::GlobalConfig>

=cut

__PACKAGE__->belongs_to(
  "global_config",
  "Breadcrumbs::Schema::Result::GlobalConfig",
  { gconf_id => "global_config_id" },
  { is_deferrable => 1, on_delete => "NO ACTION", on_update => "NO ACTION" },
);

=head2 inst

Type: belongs_to

Related object: L<Breadcrumbs::Schema::Result::Institution>

=cut

__PACKAGE__->belongs_to(
  "inst",
  "Breadcrumbs::Schema::Result::Institution",
  { inst_id => "inst_id" },
  { is_deferrable => 1, on_delete => "NO ACTION", on_update => "NO ACTION" },
);

=head2 log

Type: belongs_to

Related object: L<Breadcrumbs::Schema::Result::InstitutionLog>

=cut

__PACKAGE__->belongs_to(
  "log",
  "Breadcrumbs::Schema::Result::InstitutionLog",
  { logid => "log_id" },
  { is_deferrable => 1, on_delete => "NO ACTION", on_update => "NO ACTION" },
);

=head2 normalisation_configs

Type: has_many

Related object: L<Breadcrumbs::Schema::Result::NormalisationConfig>

=cut

__PACKAGE__->has_many(
  "normalisation_configs",
  "Breadcrumbs::Schema::Result::NormalisationConfig",
  { "foreign.inst_conf_id" => "self.iconf_id" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07039 @ 2016-01-19 13:42:06
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:BIb7ehAatKZDEcvi7rP2aQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
