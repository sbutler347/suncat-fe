use utf8;
package Breadcrumbs::Schema::Result::AuthUserUserPermission;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Breadcrumbs::Schema::Result::AuthUserUserPermission

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<auth_user_user_permissions>

=cut

__PACKAGE__->table("auth_user_user_permissions");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'auth_user_user_permissions_id_seq'

=head2 user_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 permission_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "auth_user_user_permissions_id_seq",
  },
  "user_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "permission_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<auth_user_user_permissions_user_id_14a6b632_uniq>

=over 4

=item * L</user_id>

=item * L</permission_id>

=back

=cut

__PACKAGE__->add_unique_constraint(
  "auth_user_user_permissions_user_id_14a6b632_uniq",
  ["user_id", "permission_id"],
);

=head1 RELATIONS

=head2 permission

Type: belongs_to

Related object: L<Breadcrumbs::Schema::Result::AuthPermission>

=cut

__PACKAGE__->belongs_to(
  "permission",
  "Breadcrumbs::Schema::Result::AuthPermission",
  { id => "permission_id" },
  { is_deferrable => 1, on_delete => "NO ACTION", on_update => "NO ACTION" },
);

=head2 user

Type: belongs_to

Related object: L<Breadcrumbs::Schema::Result::AuthUser>

=cut

__PACKAGE__->belongs_to(
  "user",
  "Breadcrumbs::Schema::Result::AuthUser",
  { id => "user_id" },
  { is_deferrable => 1, on_delete => "NO ACTION", on_update => "NO ACTION" },
);


# Created by DBIx::Class::Schema::Loader v0.07039 @ 2016-01-19 13:42:06
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:d43QDsIalHTP48h6wZHzNA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
