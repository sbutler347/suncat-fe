use utf8;
package Breadcrumbs::Schema::Result::NormFunctionConfig;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Breadcrumbs::Schema::Result::NormFunctionConfig

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<norm_function_config>

=cut

__PACKAGE__->table("norm_function_config");

=head1 ACCESSORS

=head2 nfconf_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: '"norm_function_config_nfConf_id_seq"'

=head2 sequence

  accessor: undef
  data_type: 'integer'
  is_nullable: 0

=head2 param_1

  data_type: 'varchar'
  is_nullable: 0
  size: 50

=head2 param_2

  data_type: 'varchar'
  is_nullable: 0
  size: 50

=head2 name

  data_type: 'varchar'
  is_nullable: 0
  size: 50

=cut

__PACKAGE__->add_columns(
  "nfconf_id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "\"norm_function_config_nfConf_id_seq\"",
  },
  "sequence",
  { accessor => undef, data_type => "integer", is_nullable => 0 },
  "param_1",
  { data_type => "varchar", is_nullable => 0, size => 50 },
  "param_2",
  { data_type => "varchar", is_nullable => 0, size => 50 },
  "name",
  { data_type => "varchar", is_nullable => 0, size => 50 },
);

=head1 PRIMARY KEY

=over 4

=item * L</nfconf_id>

=back

=cut

__PACKAGE__->set_primary_key("nfconf_id");

=head1 RELATIONS

=head2 function_maps

Type: has_many

Related object: L<Breadcrumbs::Schema::Result::FunctionMap>

=cut

__PACKAGE__->has_many(
  "function_maps",
  "Breadcrumbs::Schema::Result::FunctionMap",
  { "foreign.function_config_id" => "self.nfconf_id" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07039 @ 2016-01-19 13:42:06
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:lpvT7PUJrKdfE3zfRYjlHQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
