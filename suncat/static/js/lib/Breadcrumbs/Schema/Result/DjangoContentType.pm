use utf8;
package Breadcrumbs::Schema::Result::DjangoContentType;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Breadcrumbs::Schema::Result::DjangoContentType

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<django_content_type>

=cut

__PACKAGE__->table("django_content_type");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'django_content_type_id_seq'

=head2 app_label

  data_type: 'varchar'
  is_nullable: 0
  size: 100

=head2 model

  data_type: 'varchar'
  is_nullable: 0
  size: 100

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "django_content_type_id_seq",
  },
  "app_label",
  { data_type => "varchar", is_nullable => 0, size => 100 },
  "model",
  { data_type => "varchar", is_nullable => 0, size => 100 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<django_content_type_app_label_76bd3d3b_uniq>

=over 4

=item * L</app_label>

=item * L</model>

=back

=cut

__PACKAGE__->add_unique_constraint(
  "django_content_type_app_label_76bd3d3b_uniq",
  ["app_label", "model"],
);

=head1 RELATIONS

=head2 auth_permissions

Type: has_many

Related object: L<Breadcrumbs::Schema::Result::AuthPermission>

=cut

__PACKAGE__->has_many(
  "auth_permissions",
  "Breadcrumbs::Schema::Result::AuthPermission",
  { "foreign.content_type_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 django_admin_logs

Type: has_many

Related object: L<Breadcrumbs::Schema::Result::DjangoAdminLog>

=cut

__PACKAGE__->has_many(
  "django_admin_logs",
  "Breadcrumbs::Schema::Result::DjangoAdminLog",
  { "foreign.content_type_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07039 @ 2016-01-19 13:42:06
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:WD2yhSYFT7zZD8heW1h/EA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
