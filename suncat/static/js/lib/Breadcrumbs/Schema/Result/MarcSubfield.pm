use utf8;
package Breadcrumbs::Schema::Result::MarcSubfield;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Breadcrumbs::Schema::Result::MarcSubfield

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<marc_subfield>

=cut

__PACKAGE__->table("marc_subfield");

=head1 ACCESSORS

=head2 sf_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'marc_subfield_sf_id_seq'

=head2 subfield_code

  data_type: 'varchar'
  is_nullable: 0
  size: 1

=cut

__PACKAGE__->add_columns(
  "sf_id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "marc_subfield_sf_id_seq",
  },
  "subfield_code",
  { data_type => "varchar", is_nullable => 0, size => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</sf_id>

=back

=cut

__PACKAGE__->set_primary_key("sf_id");

=head1 RELATIONS

=head2 normalisation_config_subfield_1s

Type: has_many

Related object: L<Breadcrumbs::Schema::Result::NormalisationConfig>

=cut

__PACKAGE__->has_many(
  "normalisation_config_subfield_1s",
  "Breadcrumbs::Schema::Result::NormalisationConfig",
  { "foreign.subfield_1_id" => "self.sf_id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 normalisation_config_subfield_2s

Type: has_many

Related object: L<Breadcrumbs::Schema::Result::NormalisationConfig>

=cut

__PACKAGE__->has_many(
  "normalisation_config_subfield_2s",
  "Breadcrumbs::Schema::Result::NormalisationConfig",
  { "foreign.subfield_2_id" => "self.sf_id" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07039 @ 2016-01-19 13:42:06
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:/mpKe/h+0yLZAtfasUiBLQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
