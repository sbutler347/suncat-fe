use utf8;
package Breadcrumbs::Schema::Result::NormalisationConfigFunctionMap;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Breadcrumbs::Schema::Result::NormalisationConfigFunctionMap

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<normalisation_config_function_map>

=cut

__PACKAGE__->table("normalisation_config_function_map");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'normalisation_config_function_map_id_seq'

=head2 normalisationconfig_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 functionmap_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "normalisation_config_function_map_id_seq",
  },
  "normalisationconfig_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "functionmap_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<normalisation_config_funct_normalisationconfig_id_116e8f84_uniq>

=over 4

=item * L</normalisationconfig_id>

=item * L</functionmap_id>

=back

=cut

__PACKAGE__->add_unique_constraint(
  "normalisation_config_funct_normalisationconfig_id_116e8f84_uniq",
  ["normalisationconfig_id", "functionmap_id"],
);

=head1 RELATIONS

=head2 functionmap

Type: belongs_to

Related object: L<Breadcrumbs::Schema::Result::FunctionMap>

=cut

__PACKAGE__->belongs_to(
  "functionmap",
  "Breadcrumbs::Schema::Result::FunctionMap",
  { fm_id => "functionmap_id" },
  { is_deferrable => 1, on_delete => "NO ACTION", on_update => "NO ACTION" },
);

=head2 normalisationconfig

Type: belongs_to

Related object: L<Breadcrumbs::Schema::Result::NormalisationConfig>

=cut

__PACKAGE__->belongs_to(
  "normalisationconfig",
  "Breadcrumbs::Schema::Result::NormalisationConfig",
  { nconf_id => "normalisationconfig_id" },
  { is_deferrable => 1, on_delete => "NO ACTION", on_update => "NO ACTION" },
);


# Created by DBIx::Class::Schema::Loader v0.07039 @ 2016-01-19 13:42:06
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:uVYhl3nol37+UjQ7T1dOVg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
