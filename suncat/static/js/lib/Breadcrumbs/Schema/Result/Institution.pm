use utf8;
package Breadcrumbs::Schema::Result::Institution;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Breadcrumbs::Schema::Result::Institution

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<institution>

=cut

__PACKAGE__->table("institution");

=head1 ACCESSORS

=head2 inst_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'institution_inst_id_seq'

=head2 name

  data_type: 'varchar'
  is_nullable: 0
  size: 40

=head2 inst_code

  data_type: 'varchar'
  is_nullable: 0
  size: 5

=cut

__PACKAGE__->add_columns(
  "inst_id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "institution_inst_id_seq",
  },
  "name",
  { data_type => "varchar", is_nullable => 0, size => 40 },
  "inst_code",
  { data_type => "varchar", is_nullable => 0, size => 5 },
);

=head1 PRIMARY KEY

=over 4

=item * L</inst_id>

=back

=cut

__PACKAGE__->set_primary_key("inst_id");

=head1 RELATIONS

=head2 institution_config

Type: might_have

Related object: L<Breadcrumbs::Schema::Result::InstitutionConfig>

=cut

__PACKAGE__->might_have(
  "institution_config",
  "Breadcrumbs::Schema::Result::InstitutionConfig",
  { "foreign.inst_id" => "self.inst_id" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07039 @ 2016-01-19 13:42:06
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:cEHlzsK2aJzcRCKNacnHzA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
