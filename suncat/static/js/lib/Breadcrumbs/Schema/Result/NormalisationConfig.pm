use utf8;
package Breadcrumbs::Schema::Result::NormalisationConfig;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Breadcrumbs::Schema::Result::NormalisationConfig

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<normalisation_config>

=cut

__PACKAGE__->table("normalisation_config");

=head1 ACCESSORS

=head2 nconf_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'normalisation_config_nconf_id_seq'

=head2 inst_conf_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 marcfield_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 subfield_1_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 subfield_2_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 name

  data_type: 'varchar'
  is_nullable: 0
  size: 40

=cut

__PACKAGE__->add_columns(
  "nconf_id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "normalisation_config_nconf_id_seq",
  },
  "inst_conf_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "marcfield_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "subfield_1_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "subfield_2_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "name",
  { data_type => "varchar", is_nullable => 0, size => 40 },
);

=head1 PRIMARY KEY

=over 4

=item * L</nconf_id>

=back

=cut

__PACKAGE__->set_primary_key("nconf_id");

=head1 RELATIONS

=head2 inst_conf

Type: belongs_to

Related object: L<Breadcrumbs::Schema::Result::InstitutionConfig>

=cut

__PACKAGE__->belongs_to(
  "inst_conf",
  "Breadcrumbs::Schema::Result::InstitutionConfig",
  { iconf_id => "inst_conf_id" },
  {
    is_deferrable => 1,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "NO ACTION",
  },
);

=head2 marcfield

Type: belongs_to

Related object: L<Breadcrumbs::Schema::Result::MarcField>

=cut

__PACKAGE__->belongs_to(
  "marcfield",
  "Breadcrumbs::Schema::Result::MarcField",
  { mf_id => "marcfield_id" },
  {
    is_deferrable => 1,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "NO ACTION",
  },
);

=head2 normalisation_config_function_maps

Type: has_many

Related object: L<Breadcrumbs::Schema::Result::NormalisationConfigFunctionMap>

=cut

__PACKAGE__->has_many(
  "normalisation_config_function_maps",
  "Breadcrumbs::Schema::Result::NormalisationConfigFunctionMap",
  { "foreign.normalisationconfig_id" => "self.nconf_id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 subfield_1

Type: belongs_to

Related object: L<Breadcrumbs::Schema::Result::MarcSubfield>

=cut

__PACKAGE__->belongs_to(
  "subfield_1",
  "Breadcrumbs::Schema::Result::MarcSubfield",
  { sf_id => "subfield_1_id" },
  {
    is_deferrable => 1,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "NO ACTION",
  },
);

=head2 subfield_2

Type: belongs_to

Related object: L<Breadcrumbs::Schema::Result::MarcSubfield>

=cut

__PACKAGE__->belongs_to(
  "subfield_2",
  "Breadcrumbs::Schema::Result::MarcSubfield",
  { sf_id => "subfield_2_id" },
  {
    is_deferrable => 1,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "NO ACTION",
  },
);


# Created by DBIx::Class::Schema::Loader v0.07039 @ 2016-01-19 13:42:06
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:gpsb2ikGD1w+v7Ml+6gayg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
