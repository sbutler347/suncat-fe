use utf8;
package Breadcrumbs::Schema::Result::GlobalConfig;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Breadcrumbs::Schema::Result::GlobalConfig

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<global_config>

=cut

__PACKAGE__->table("global_config");

=head1 ACCESSORS

=head2 gconf_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'global_config_gconf_id_seq'

=head2 name

  data_type: 'varchar'
  is_nullable: 0
  size: 30

=cut

__PACKAGE__->add_columns(
  "gconf_id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "global_config_gconf_id_seq",
  },
  "name",
  { data_type => "varchar", is_nullable => 0, size => 30 },
);

=head1 PRIMARY KEY

=over 4

=item * L</gconf_id>

=back

=cut

__PACKAGE__->set_primary_key("gconf_id");

=head1 RELATIONS

=head2 institution_configs

Type: has_many

Related object: L<Breadcrumbs::Schema::Result::InstitutionConfig>

=cut

__PACKAGE__->has_many(
  "institution_configs",
  "Breadcrumbs::Schema::Result::InstitutionConfig",
  { "foreign.global_config_id" => "self.gconf_id" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07039 @ 2016-01-19 13:42:06
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:Wj2k44pM/LwtUd2fX4DRdw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
