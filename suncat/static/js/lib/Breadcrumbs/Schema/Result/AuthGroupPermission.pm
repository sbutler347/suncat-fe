use utf8;
package Breadcrumbs::Schema::Result::AuthGroupPermission;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Breadcrumbs::Schema::Result::AuthGroupPermission

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<auth_group_permissions>

=cut

__PACKAGE__->table("auth_group_permissions");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'auth_group_permissions_id_seq'

=head2 group_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 permission_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "auth_group_permissions_id_seq",
  },
  "group_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "permission_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<auth_group_permissions_group_id_0cd325b0_uniq>

=over 4

=item * L</group_id>

=item * L</permission_id>

=back

=cut

__PACKAGE__->add_unique_constraint(
  "auth_group_permissions_group_id_0cd325b0_uniq",
  ["group_id", "permission_id"],
);

=head1 RELATIONS

=head2 group

Type: belongs_to

Related object: L<Breadcrumbs::Schema::Result::AuthGroup>

=cut

__PACKAGE__->belongs_to(
  "group",
  "Breadcrumbs::Schema::Result::AuthGroup",
  { id => "group_id" },
  { is_deferrable => 1, on_delete => "NO ACTION", on_update => "NO ACTION" },
);

=head2 permission

Type: belongs_to

Related object: L<Breadcrumbs::Schema::Result::AuthPermission>

=cut

__PACKAGE__->belongs_to(
  "permission",
  "Breadcrumbs::Schema::Result::AuthPermission",
  { id => "permission_id" },
  { is_deferrable => 1, on_delete => "NO ACTION", on_update => "NO ACTION" },
);


# Created by DBIx::Class::Schema::Loader v0.07039 @ 2016-01-19 13:42:06
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:PpEPNT1w/j0cnpxviUbsig


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
