use utf8;
package Breadcrumbs::Schema::Result::MarcField;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Breadcrumbs::Schema::Result::MarcField

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<marc_field>

=cut

__PACKAGE__->table("marc_field");

=head1 ACCESSORS

=head2 mf_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'marc_field_mf_id_seq'

=head2 marc_code

  data_type: 'varchar'
  is_nullable: 0
  size: 3

=cut

__PACKAGE__->add_columns(
  "mf_id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "marc_field_mf_id_seq",
  },
  "marc_code",
  { data_type => "varchar", is_nullable => 0, size => 3 },
);

=head1 PRIMARY KEY

=over 4

=item * L</mf_id>

=back

=cut

__PACKAGE__->set_primary_key("mf_id");

=head1 RELATIONS

=head2 normalisation_configs

Type: has_many

Related object: L<Breadcrumbs::Schema::Result::NormalisationConfig>

=cut

__PACKAGE__->has_many(
  "normalisation_configs",
  "Breadcrumbs::Schema::Result::NormalisationConfig",
  { "foreign.marcfield_id" => "self.mf_id" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07039 @ 2016-01-19 13:42:06
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:jrjX6dOC/X8AAvVhqr64rQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
