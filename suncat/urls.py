from django.conf.urls import url
from django.conf import settings
from django.contrib import admin
from django.conf.urls.static import static

from . import views

app_name = 'suncat'
urlpatterns = [
    url(r'^.*$', admin.site.urls),

]+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)