PGDMP                         t           suncat_backend    9.3.9    9.3.9 �    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            �           1262    109998    suncat_backend    DATABASE     �   CREATE DATABASE suncat_backend WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_GB.UTF-8' LC_CTYPE = 'en_GB.UTF-8';
    DROP DATABASE suncat_backend;
             postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            �           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    5            �           0    0    public    ACL     �   REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;
                  postgres    false    5            �            3079    11787    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            �           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    213            �            1259    110030 
   auth_group    TABLE     ^   CREATE TABLE auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);
    DROP TABLE public.auth_group;
       public         suncat_db_admin    false    5            �            1259    110028    auth_group_id_seq    SEQUENCE     s   CREATE SEQUENCE auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.auth_group_id_seq;
       public       suncat_db_admin    false    177    5            �           0    0    auth_group_id_seq    SEQUENCE OWNED BY     9   ALTER SEQUENCE auth_group_id_seq OWNED BY auth_group.id;
            public       suncat_db_admin    false    176            �            1259    110040    auth_group_permissions    TABLE     �   CREATE TABLE auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);
 *   DROP TABLE public.auth_group_permissions;
       public         suncat_db_admin    false    5            �            1259    110038    auth_group_permissions_id_seq    SEQUENCE        CREATE SEQUENCE auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE public.auth_group_permissions_id_seq;
       public       suncat_db_admin    false    179    5            �           0    0    auth_group_permissions_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE auth_group_permissions_id_seq OWNED BY auth_group_permissions.id;
            public       suncat_db_admin    false    178            �            1259    110022    auth_permission    TABLE     �   CREATE TABLE auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);
 #   DROP TABLE public.auth_permission;
       public         suncat_db_admin    false    5            �            1259    110020    auth_permission_id_seq    SEQUENCE     x   CREATE SEQUENCE auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.auth_permission_id_seq;
       public       suncat_db_admin    false    175    5            �           0    0    auth_permission_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE auth_permission_id_seq OWNED BY auth_permission.id;
            public       suncat_db_admin    false    174            �            1259    110048 	   auth_user    TABLE     �  CREATE TABLE auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(30) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);
    DROP TABLE public.auth_user;
       public         suncat_db_admin    false    5            �            1259    110058    auth_user_groups    TABLE     x   CREATE TABLE auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);
 $   DROP TABLE public.auth_user_groups;
       public         suncat_db_admin    false    5            �            1259    110056    auth_user_groups_id_seq    SEQUENCE     y   CREATE SEQUENCE auth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.auth_user_groups_id_seq;
       public       suncat_db_admin    false    183    5            �           0    0    auth_user_groups_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE auth_user_groups_id_seq OWNED BY auth_user_groups.id;
            public       suncat_db_admin    false    182            �            1259    110046    auth_user_id_seq    SEQUENCE     r   CREATE SEQUENCE auth_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.auth_user_id_seq;
       public       suncat_db_admin    false    181    5            �           0    0    auth_user_id_seq    SEQUENCE OWNED BY     7   ALTER SEQUENCE auth_user_id_seq OWNED BY auth_user.id;
            public       suncat_db_admin    false    180            �            1259    110066    auth_user_user_permissions    TABLE     �   CREATE TABLE auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);
 .   DROP TABLE public.auth_user_user_permissions;
       public         suncat_db_admin    false    5            �            1259    110064 !   auth_user_user_permissions_id_seq    SEQUENCE     �   CREATE SEQUENCE auth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 8   DROP SEQUENCE public.auth_user_user_permissions_id_seq;
       public       suncat_db_admin    false    5    185             	           0    0 !   auth_user_user_permissions_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE auth_user_user_permissions_id_seq OWNED BY auth_user_user_permissions.id;
            public       suncat_db_admin    false    184            �            1259    110126    django_admin_log    TABLE     �  CREATE TABLE django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);
 $   DROP TABLE public.django_admin_log;
       public         suncat_db_admin    false    5            �            1259    110124    django_admin_log_id_seq    SEQUENCE     y   CREATE SEQUENCE django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.django_admin_log_id_seq;
       public       suncat_db_admin    false    187    5            	           0    0    django_admin_log_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE django_admin_log_id_seq OWNED BY django_admin_log.id;
            public       suncat_db_admin    false    186            �            1259    110012    django_content_type    TABLE     �   CREATE TABLE django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);
 '   DROP TABLE public.django_content_type;
       public         suncat_db_admin    false    5            �            1259    110010    django_content_type_id_seq    SEQUENCE     |   CREATE SEQUENCE django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.django_content_type_id_seq;
       public       suncat_db_admin    false    173    5            	           0    0    django_content_type_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE django_content_type_id_seq OWNED BY django_content_type.id;
            public       suncat_db_admin    false    172            �            1259    110001    django_migrations    TABLE     �   CREATE TABLE django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);
 %   DROP TABLE public.django_migrations;
       public         suncat_db_admin    false    5            �            1259    109999    django_migrations_id_seq    SEQUENCE     z   CREATE SEQUENCE django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.django_migrations_id_seq;
       public       suncat_db_admin    false    5    171            	           0    0    django_migrations_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE django_migrations_id_seq OWNED BY django_migrations.id;
            public       suncat_db_admin    false    170            �            1259    110204    django_session    TABLE     �   CREATE TABLE django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);
 "   DROP TABLE public.django_session;
       public         suncat_db_admin    false    5            �            1259    110151    global_config    TABLE     g   CREATE TABLE global_config (
    gconf_id integer NOT NULL,
    name character varying(50) NOT NULL
);
 !   DROP TABLE public.global_config;
       public         suncat_db_admin    false    5            �            1259    110149    global_config_gconf_id_seq    SEQUENCE     |   CREATE SEQUENCE global_config_gconf_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.global_config_gconf_id_seq;
       public       suncat_db_admin    false    5    189            	           0    0    global_config_gconf_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE global_config_gconf_id_seq OWNED BY global_config.gconf_id;
            public       suncat_db_admin    false    188            �            1259    110276    holdings_normalisation_config    TABLE     �  CREATE TABLE holdings_normalisation_config (
    nconf_id integer NOT NULL,
    transition_step boolean NOT NULL,
    function_sequence character varying(10) NOT NULL,
    ind_1 character varying(1) NOT NULL,
    ind_2 character varying(1) NOT NULL,
    param_1 character varying(50) NOT NULL,
    param_2 character varying(50) NOT NULL,
    function_id integer NOT NULL,
    group_sequence_id integer,
    inst_conf_id integer,
    marcfield_id integer,
    subfield_1_id integer,
    subfield_2_id integer
);
 1   DROP TABLE public.holdings_normalisation_config;
       public         suncat_db_admin    false    5            �            1259    110274 *   holdings_normalisation_config_nconf_id_seq    SEQUENCE     �   CREATE SEQUENCE holdings_normalisation_config_nconf_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 A   DROP SEQUENCE public.holdings_normalisation_config_nconf_id_seq;
       public       suncat_db_admin    false    5    206            	           0    0 *   holdings_normalisation_config_nconf_id_seq    SEQUENCE OWNED BY     k   ALTER SEQUENCE holdings_normalisation_config_nconf_id_seq OWNED BY holdings_normalisation_config.nconf_id;
            public       suncat_db_admin    false    205            �            1259    110159    institution_config    TABLE     �  CREATE TABLE institution_config (
    iconf_id integer NOT NULL,
    name character varying(50) NOT NULL,
    inst_code character varying(5) NOT NULL,
    ingest_file_format character varying(20) NOT NULL,
    bib_mapping_field character varying(3) NOT NULL,
    bib_mapping_subfield character varying(1) NOT NULL,
    hol_mapping_field character varying(3) NOT NULL,
    hol_mapping_subfield character varying(1) NOT NULL,
    global_config_id integer NOT NULL
);
 &   DROP TABLE public.institution_config;
       public         suncat_db_admin    false    5            �            1259    110157    institution_config_iconf_id_seq    SEQUENCE     �   CREATE SEQUENCE institution_config_iconf_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 6   DROP SEQUENCE public.institution_config_iconf_id_seq;
       public       suncat_db_admin    false    191    5            	           0    0    institution_config_iconf_id_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE institution_config_iconf_id_seq OWNED BY institution_config.iconf_id;
            public       suncat_db_admin    false    190            �            1259    110169    institution_log    TABLE     �   CREATE TABLE institution_log (
    logid integer NOT NULL,
    logdate timestamp with time zone NOT NULL,
    logfile character varying(50) NOT NULL
);
 #   DROP TABLE public.institution_log;
       public         suncat_db_admin    false    5            �            1259    110167    institution_log_logid_seq    SEQUENCE     {   CREATE SEQUENCE institution_log_logid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.institution_log_logid_seq;
       public       suncat_db_admin    false    5    193            	           0    0    institution_log_logid_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE institution_log_logid_seq OWNED BY institution_log.logid;
            public       suncat_db_admin    false    192            �            1259    110177 
   marc_field    TABLE     e   CREATE TABLE marc_field (
    mf_id integer NOT NULL,
    marc_code character varying(6) NOT NULL
);
    DROP TABLE public.marc_field;
       public         suncat_db_admin    false    5            �            1259    110175    marc_field_mf_id_seq    SEQUENCE     v   CREATE SEQUENCE marc_field_mf_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.marc_field_mf_id_seq;
       public       suncat_db_admin    false    5    195            	           0    0    marc_field_mf_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE marc_field_mf_id_seq OWNED BY marc_field.mf_id;
            public       suncat_db_admin    false    194            �            1259    110187    marc_subfield    TABLE     l   CREATE TABLE marc_subfield (
    sf_id integer NOT NULL,
    subfield_code character varying(1) NOT NULL
);
 !   DROP TABLE public.marc_subfield;
       public         suncat_db_admin    false    5            �            1259    110185    marc_subfield_sf_id_seq    SEQUENCE     y   CREATE SEQUENCE marc_subfield_sf_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.marc_subfield_sf_id_seq;
       public       suncat_db_admin    false    5    197            		           0    0    marc_subfield_sf_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE marc_subfield_sf_id_seq OWNED BY marc_subfield.sf_id;
            public       suncat_db_admin    false    196            �            1259    110284    normalisation_config    TABLE     �  CREATE TABLE normalisation_config (
    nconf_id integer NOT NULL,
    transition_step boolean NOT NULL,
    function_sequence character varying(10) NOT NULL,
    ind_1 character varying(1) NOT NULL,
    ind_2 character varying(1) NOT NULL,
    param_1 character varying(50) NOT NULL,
    param_2 character varying(50) NOT NULL,
    function_id integer NOT NULL,
    group_sequence_id integer,
    inst_conf_id integer,
    marcfield_id integer,
    subfield_1_id integer,
    subfield_2_id integer
);
 (   DROP TABLE public.normalisation_config;
       public         suncat_db_admin    false    5            �            1259    110282 !   normalisation_config_nconf_id_seq    SEQUENCE     �   CREATE SEQUENCE normalisation_config_nconf_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 8   DROP SEQUENCE public.normalisation_config_nconf_id_seq;
       public       suncat_db_admin    false    5    208            
	           0    0 !   normalisation_config_nconf_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE normalisation_config_nconf_id_seq OWNED BY normalisation_config.nconf_id;
            public       suncat_db_admin    false    207            �            1259    110300    normalisation_functions    TABLE     �   CREATE TABLE normalisation_functions (
    func_id integer NOT NULL,
    display_name character varying(50) NOT NULL,
    function_name character varying(40) NOT NULL
);
 +   DROP TABLE public.normalisation_functions;
       public         suncat_db_admin    false    5            �            1259    110298 #   normalisation_functions_func_id_seq    SEQUENCE     �   CREATE SEQUENCE normalisation_functions_func_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 :   DROP SEQUENCE public.normalisation_functions_func_id_seq;
       public       suncat_db_admin    false    212    5            	           0    0 #   normalisation_functions_func_id_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE normalisation_functions_func_id_seq OWNED BY normalisation_functions.func_id;
            public       suncat_db_admin    false    211            �            1259    110292    normalisation_group    TABLE     d   CREATE TABLE normalisation_group (
    func_id integer NOT NULL,
    group_name integer NOT NULL
);
 '   DROP TABLE public.normalisation_group;
       public         suncat_db_admin    false    5            �            1259    110290    normalisation_group_func_id_seq    SEQUENCE     �   CREATE SEQUENCE normalisation_group_func_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 6   DROP SEQUENCE public.normalisation_group_func_id_seq;
       public       suncat_db_admin    false    5    210            	           0    0    normalisation_group_func_id_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE normalisation_group_func_id_seq OWNED BY normalisation_group.func_id;
            public       suncat_db_admin    false    209            �            1259    110216    skip_config    TABLE     �  CREATE TABLE skip_config (
    skipconf_id integer NOT NULL,
    skip_type character varying(6) NOT NULL,
    ind_1 character varying(1) NOT NULL,
    ind_2 character varying(1) NOT NULL,
    skip_param_1 character varying(50) NOT NULL,
    condition_marcfield_id integer,
    skip_function_id integer NOT NULL,
    skip_group_id integer,
    skip_inst_conf_id integer,
    skip_marcfield_id integer,
    skip_subfield_1_id integer
);
    DROP TABLE public.skip_config;
       public         suncat_db_admin    false    5            �            1259    110214    skip_config_skipconf_id_seq    SEQUENCE     }   CREATE SEQUENCE skip_config_skipconf_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 2   DROP SEQUENCE public.skip_config_skipconf_id_seq;
       public       suncat_db_admin    false    5    200            	           0    0    skip_config_skipconf_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE skip_config_skipconf_id_seq OWNED BY skip_config.skipconf_id;
            public       suncat_db_admin    false    199            �            1259    110224    skip_functions    TABLE     �   CREATE TABLE skip_functions (
    func_id integer NOT NULL,
    display_name character varying(50) NOT NULL,
    function_name character varying(40) NOT NULL
);
 "   DROP TABLE public.skip_functions;
       public         suncat_db_admin    false    5            �            1259    110222    skip_functions_func_id_seq    SEQUENCE     |   CREATE SEQUENCE skip_functions_func_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.skip_functions_func_id_seq;
       public       suncat_db_admin    false    5    202            	           0    0    skip_functions_func_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE skip_functions_func_id_seq OWNED BY skip_functions.func_id;
            public       suncat_db_admin    false    201            �            1259    110232 
   skip_group    TABLE     [   CREATE TABLE skip_group (
    func_id integer NOT NULL,
    group_name integer NOT NULL
);
    DROP TABLE public.skip_group;
       public         suncat_db_admin    false    5            �            1259    110230    skip_group_func_id_seq    SEQUENCE     x   CREATE SEQUENCE skip_group_func_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.skip_group_func_id_seq;
       public       suncat_db_admin    false    204    5            	           0    0    skip_group_func_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE skip_group_func_id_seq OWNED BY skip_group.func_id;
            public       suncat_db_admin    false    203            �           2604    110033    id    DEFAULT     `   ALTER TABLE ONLY auth_group ALTER COLUMN id SET DEFAULT nextval('auth_group_id_seq'::regclass);
 <   ALTER TABLE public.auth_group ALTER COLUMN id DROP DEFAULT;
       public       suncat_db_admin    false    176    177    177            �           2604    110043    id    DEFAULT     x   ALTER TABLE ONLY auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('auth_group_permissions_id_seq'::regclass);
 H   ALTER TABLE public.auth_group_permissions ALTER COLUMN id DROP DEFAULT;
       public       suncat_db_admin    false    179    178    179            �           2604    110025    id    DEFAULT     j   ALTER TABLE ONLY auth_permission ALTER COLUMN id SET DEFAULT nextval('auth_permission_id_seq'::regclass);
 A   ALTER TABLE public.auth_permission ALTER COLUMN id DROP DEFAULT;
       public       suncat_db_admin    false    175    174    175            �           2604    110051    id    DEFAULT     ^   ALTER TABLE ONLY auth_user ALTER COLUMN id SET DEFAULT nextval('auth_user_id_seq'::regclass);
 ;   ALTER TABLE public.auth_user ALTER COLUMN id DROP DEFAULT;
       public       suncat_db_admin    false    181    180    181            �           2604    110061    id    DEFAULT     l   ALTER TABLE ONLY auth_user_groups ALTER COLUMN id SET DEFAULT nextval('auth_user_groups_id_seq'::regclass);
 B   ALTER TABLE public.auth_user_groups ALTER COLUMN id DROP DEFAULT;
       public       suncat_db_admin    false    182    183    183            �           2604    110069    id    DEFAULT     �   ALTER TABLE ONLY auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('auth_user_user_permissions_id_seq'::regclass);
 L   ALTER TABLE public.auth_user_user_permissions ALTER COLUMN id DROP DEFAULT;
       public       suncat_db_admin    false    185    184    185            �           2604    110129    id    DEFAULT     l   ALTER TABLE ONLY django_admin_log ALTER COLUMN id SET DEFAULT nextval('django_admin_log_id_seq'::regclass);
 B   ALTER TABLE public.django_admin_log ALTER COLUMN id DROP DEFAULT;
       public       suncat_db_admin    false    187    186    187            �           2604    110015    id    DEFAULT     r   ALTER TABLE ONLY django_content_type ALTER COLUMN id SET DEFAULT nextval('django_content_type_id_seq'::regclass);
 E   ALTER TABLE public.django_content_type ALTER COLUMN id DROP DEFAULT;
       public       suncat_db_admin    false    172    173    173            �           2604    110004    id    DEFAULT     n   ALTER TABLE ONLY django_migrations ALTER COLUMN id SET DEFAULT nextval('django_migrations_id_seq'::regclass);
 C   ALTER TABLE public.django_migrations ALTER COLUMN id DROP DEFAULT;
       public       suncat_db_admin    false    171    170    171            �           2604    110154    gconf_id    DEFAULT     r   ALTER TABLE ONLY global_config ALTER COLUMN gconf_id SET DEFAULT nextval('global_config_gconf_id_seq'::regclass);
 E   ALTER TABLE public.global_config ALTER COLUMN gconf_id DROP DEFAULT;
       public       suncat_db_admin    false    189    188    189            �           2604    110279    nconf_id    DEFAULT     �   ALTER TABLE ONLY holdings_normalisation_config ALTER COLUMN nconf_id SET DEFAULT nextval('holdings_normalisation_config_nconf_id_seq'::regclass);
 U   ALTER TABLE public.holdings_normalisation_config ALTER COLUMN nconf_id DROP DEFAULT;
       public       suncat_db_admin    false    206    205    206            �           2604    110162    iconf_id    DEFAULT     |   ALTER TABLE ONLY institution_config ALTER COLUMN iconf_id SET DEFAULT nextval('institution_config_iconf_id_seq'::regclass);
 J   ALTER TABLE public.institution_config ALTER COLUMN iconf_id DROP DEFAULT;
       public       suncat_db_admin    false    190    191    191            �           2604    110172    logid    DEFAULT     p   ALTER TABLE ONLY institution_log ALTER COLUMN logid SET DEFAULT nextval('institution_log_logid_seq'::regclass);
 D   ALTER TABLE public.institution_log ALTER COLUMN logid DROP DEFAULT;
       public       suncat_db_admin    false    192    193    193            �           2604    110180    mf_id    DEFAULT     f   ALTER TABLE ONLY marc_field ALTER COLUMN mf_id SET DEFAULT nextval('marc_field_mf_id_seq'::regclass);
 ?   ALTER TABLE public.marc_field ALTER COLUMN mf_id DROP DEFAULT;
       public       suncat_db_admin    false    195    194    195            �           2604    110190    sf_id    DEFAULT     l   ALTER TABLE ONLY marc_subfield ALTER COLUMN sf_id SET DEFAULT nextval('marc_subfield_sf_id_seq'::regclass);
 B   ALTER TABLE public.marc_subfield ALTER COLUMN sf_id DROP DEFAULT;
       public       suncat_db_admin    false    196    197    197            �           2604    110287    nconf_id    DEFAULT     �   ALTER TABLE ONLY normalisation_config ALTER COLUMN nconf_id SET DEFAULT nextval('normalisation_config_nconf_id_seq'::regclass);
 L   ALTER TABLE public.normalisation_config ALTER COLUMN nconf_id DROP DEFAULT;
       public       suncat_db_admin    false    208    207    208            �           2604    110303    func_id    DEFAULT     �   ALTER TABLE ONLY normalisation_functions ALTER COLUMN func_id SET DEFAULT nextval('normalisation_functions_func_id_seq'::regclass);
 N   ALTER TABLE public.normalisation_functions ALTER COLUMN func_id DROP DEFAULT;
       public       suncat_db_admin    false    212    211    212            �           2604    110295    func_id    DEFAULT     |   ALTER TABLE ONLY normalisation_group ALTER COLUMN func_id SET DEFAULT nextval('normalisation_group_func_id_seq'::regclass);
 J   ALTER TABLE public.normalisation_group ALTER COLUMN func_id DROP DEFAULT;
       public       suncat_db_admin    false    210    209    210            �           2604    110219    skipconf_id    DEFAULT     t   ALTER TABLE ONLY skip_config ALTER COLUMN skipconf_id SET DEFAULT nextval('skip_config_skipconf_id_seq'::regclass);
 F   ALTER TABLE public.skip_config ALTER COLUMN skipconf_id DROP DEFAULT;
       public       suncat_db_admin    false    199    200    200            �           2604    110227    func_id    DEFAULT     r   ALTER TABLE ONLY skip_functions ALTER COLUMN func_id SET DEFAULT nextval('skip_functions_func_id_seq'::regclass);
 E   ALTER TABLE public.skip_functions ALTER COLUMN func_id DROP DEFAULT;
       public       suncat_db_admin    false    201    202    202            �           2604    110235    func_id    DEFAULT     j   ALTER TABLE ONLY skip_group ALTER COLUMN func_id SET DEFAULT nextval('skip_group_func_id_seq'::regclass);
 A   ALTER TABLE public.skip_group ALTER COLUMN func_id DROP DEFAULT;
       public       suncat_db_admin    false    203    204    204            �          0    110030 
   auth_group 
   TABLE DATA               '   COPY auth_group (id, name) FROM stdin;
    public       suncat_db_admin    false    177   �6      	           0    0    auth_group_id_seq    SEQUENCE SET     9   SELECT pg_catalog.setval('auth_group_id_seq', 1, false);
            public       suncat_db_admin    false    176            �          0    110040    auth_group_permissions 
   TABLE DATA               F   COPY auth_group_permissions (id, group_id, permission_id) FROM stdin;
    public       suncat_db_admin    false    179   �6      	           0    0    auth_group_permissions_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('auth_group_permissions_id_seq', 1, false);
            public       suncat_db_admin    false    178            �          0    110022    auth_permission 
   TABLE DATA               G   COPY auth_permission (id, name, content_type_id, codename) FROM stdin;
    public       suncat_db_admin    false    175   7      	           0    0    auth_permission_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('auth_permission_id_seq', 54, true);
            public       suncat_db_admin    false    174            �          0    110048 	   auth_user 
   TABLE DATA               �   COPY auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
    public       suncat_db_admin    false    181   �9      �          0    110058    auth_user_groups 
   TABLE DATA               :   COPY auth_user_groups (id, user_id, group_id) FROM stdin;
    public       suncat_db_admin    false    183   ::      	           0    0    auth_user_groups_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('auth_user_groups_id_seq', 1, false);
            public       suncat_db_admin    false    182            	           0    0    auth_user_id_seq    SEQUENCE SET     7   SELECT pg_catalog.setval('auth_user_id_seq', 1, true);
            public       suncat_db_admin    false    180            �          0    110066    auth_user_user_permissions 
   TABLE DATA               I   COPY auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
    public       suncat_db_admin    false    185   W:      	           0    0 !   auth_user_user_permissions_id_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('auth_user_user_permissions_id_seq', 1, false);
            public       suncat_db_admin    false    184            �          0    110126    django_admin_log 
   TABLE DATA               �   COPY django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
    public       suncat_db_admin    false    187   t:      	           0    0    django_admin_log_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('django_admin_log_id_seq', 33, true);
            public       suncat_db_admin    false    186            �          0    110012    django_content_type 
   TABLE DATA               <   COPY django_content_type (id, app_label, model) FROM stdin;
    public       suncat_db_admin    false    173   H>      	           0    0    django_content_type_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('django_content_type_id_seq', 18, true);
            public       suncat_db_admin    false    172            �          0    110001    django_migrations 
   TABLE DATA               <   COPY django_migrations (id, app, name, applied) FROM stdin;
    public       suncat_db_admin    false    171   !?      	           0    0    django_migrations_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('django_migrations_id_seq', 15, true);
            public       suncat_db_admin    false    170            �          0    110204    django_session 
   TABLE DATA               I   COPY django_session (session_key, session_data, expire_date) FROM stdin;
    public       suncat_db_admin    false    198   �@      �          0    110151    global_config 
   TABLE DATA               0   COPY global_config (gconf_id, name) FROM stdin;
    public       suncat_db_admin    false    189   B      	           0    0    global_config_gconf_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('global_config_gconf_id_seq', 1, false);
            public       suncat_db_admin    false    188            �          0    110276    holdings_normalisation_config 
   TABLE DATA               �   COPY holdings_normalisation_config (nconf_id, transition_step, function_sequence, ind_1, ind_2, param_1, param_2, function_id, group_sequence_id, inst_conf_id, marcfield_id, subfield_1_id, subfield_2_id) FROM stdin;
    public       suncat_db_admin    false    206   4B      	           0    0 *   holdings_normalisation_config_nconf_id_seq    SEQUENCE SET     Q   SELECT pg_catalog.setval('holdings_normalisation_config_nconf_id_seq', 1, true);
            public       suncat_db_admin    false    205            �          0    110159    institution_config 
   TABLE DATA               �   COPY institution_config (iconf_id, name, inst_code, ingest_file_format, bib_mapping_field, bib_mapping_subfield, hol_mapping_field, hol_mapping_subfield, global_config_id) FROM stdin;
    public       suncat_db_admin    false    191   uB      	           0    0    institution_config_iconf_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('institution_config_iconf_id_seq', 1, false);
            public       suncat_db_admin    false    190            �          0    110169    institution_log 
   TABLE DATA               ;   COPY institution_log (logid, logdate, logfile) FROM stdin;
    public       suncat_db_admin    false    193   C      	           0    0    institution_log_logid_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('institution_log_logid_seq', 1, false);
            public       suncat_db_admin    false    192            �          0    110177 
   marc_field 
   TABLE DATA               /   COPY marc_field (mf_id, marc_code) FROM stdin;
    public       suncat_db_admin    false    195   /C      	           0    0    marc_field_mf_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('marc_field_mf_id_seq', 2, true);
            public       suncat_db_admin    false    194            �          0    110187    marc_subfield 
   TABLE DATA               6   COPY marc_subfield (sf_id, subfield_code) FROM stdin;
    public       suncat_db_admin    false    197   *D      	           0    0    marc_subfield_sf_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('marc_subfield_sf_id_seq', 1, false);
            public       suncat_db_admin    false    196            �          0    110284    normalisation_config 
   TABLE DATA               �   COPY normalisation_config (nconf_id, transition_step, function_sequence, ind_1, ind_2, param_1, param_2, function_id, group_sequence_id, inst_conf_id, marcfield_id, subfield_1_id, subfield_2_id) FROM stdin;
    public       suncat_db_admin    false    208   �D      	           0    0 !   normalisation_config_nconf_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('normalisation_config_nconf_id_seq', 5, true);
            public       suncat_db_admin    false    207            �          0    110300    normalisation_functions 
   TABLE DATA               P   COPY normalisation_functions (func_id, display_name, function_name) FROM stdin;
    public       suncat_db_admin    false    212   �F       	           0    0 #   normalisation_functions_func_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('normalisation_functions_func_id_seq', 1, false);
            public       suncat_db_admin    false    211            �          0    110292    normalisation_group 
   TABLE DATA               ;   COPY normalisation_group (func_id, group_name) FROM stdin;
    public       suncat_db_admin    false    210   dJ      !	           0    0    normalisation_group_func_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('normalisation_group_func_id_seq', 1, false);
            public       suncat_db_admin    false    209            �          0    110216    skip_config 
   TABLE DATA               �   COPY skip_config (skipconf_id, skip_type, ind_1, ind_2, skip_param_1, condition_marcfield_id, skip_function_id, skip_group_id, skip_inst_conf_id, skip_marcfield_id, skip_subfield_1_id) FROM stdin;
    public       suncat_db_admin    false    200   �J      "	           0    0    skip_config_skipconf_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('skip_config_skipconf_id_seq', 1, false);
            public       suncat_db_admin    false    199            �          0    110224    skip_functions 
   TABLE DATA               G   COPY skip_functions (func_id, display_name, function_name) FROM stdin;
    public       suncat_db_admin    false    202   L      #	           0    0    skip_functions_func_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('skip_functions_func_id_seq', 1, false);
            public       suncat_db_admin    false    201            �          0    110232 
   skip_group 
   TABLE DATA               2   COPY skip_group (func_id, group_name) FROM stdin;
    public       suncat_db_admin    false    204   "M      $	           0    0    skip_group_func_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('skip_group_func_id_seq', 1, false);
            public       suncat_db_admin    false    203            �           2606    110037    auth_group_name_key 
   CONSTRAINT     R   ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);
 H   ALTER TABLE ONLY public.auth_group DROP CONSTRAINT auth_group_name_key;
       public         suncat_db_admin    false    177    177            �           2606    110092 -   auth_group_permissions_group_id_0cd325b0_uniq 
   CONSTRAINT     �   ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_0cd325b0_uniq UNIQUE (group_id, permission_id);
 n   ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissions_group_id_0cd325b0_uniq;
       public         suncat_db_admin    false    179    179    179            �           2606    110045    auth_group_permissions_pkey 
   CONSTRAINT     i   ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);
 \   ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissions_pkey;
       public         suncat_db_admin    false    179    179            �           2606    110035    auth_group_pkey 
   CONSTRAINT     Q   ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.auth_group DROP CONSTRAINT auth_group_pkey;
       public         suncat_db_admin    false    177    177            �           2606    110078 -   auth_permission_content_type_id_01ab375a_uniq 
   CONSTRAINT     �   ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_01ab375a_uniq UNIQUE (content_type_id, codename);
 g   ALTER TABLE ONLY public.auth_permission DROP CONSTRAINT auth_permission_content_type_id_01ab375a_uniq;
       public         suncat_db_admin    false    175    175    175            �           2606    110027    auth_permission_pkey 
   CONSTRAINT     [   ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.auth_permission DROP CONSTRAINT auth_permission_pkey;
       public         suncat_db_admin    false    175    175            �           2606    110063    auth_user_groups_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_pkey;
       public         suncat_db_admin    false    183    183            �           2606    110107 &   auth_user_groups_user_id_94350c0c_uniq 
   CONSTRAINT     x   ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_94350c0c_uniq UNIQUE (user_id, group_id);
 a   ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_user_id_94350c0c_uniq;
       public         suncat_db_admin    false    183    183    183            �           2606    110053    auth_user_pkey 
   CONSTRAINT     O   ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.auth_user DROP CONSTRAINT auth_user_pkey;
       public         suncat_db_admin    false    181    181            �           2606    110071    auth_user_user_permissions_pkey 
   CONSTRAINT     q   ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);
 d   ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permissions_pkey;
       public         suncat_db_admin    false    185    185            �           2606    110121 0   auth_user_user_permissions_user_id_14a6b632_uniq 
   CONSTRAINT     �   ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_14a6b632_uniq UNIQUE (user_id, permission_id);
 u   ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permissions_user_id_14a6b632_uniq;
       public         suncat_db_admin    false    185    185    185            �           2606    110055    auth_user_username_key 
   CONSTRAINT     X   ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);
 J   ALTER TABLE ONLY public.auth_user DROP CONSTRAINT auth_user_username_key;
       public         suncat_db_admin    false    181    181                       2606    110135    django_admin_log_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.django_admin_log DROP CONSTRAINT django_admin_log_pkey;
       public         suncat_db_admin    false    187    187            �           2606    110019 +   django_content_type_app_label_76bd3d3b_uniq 
   CONSTRAINT        ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_app_label_76bd3d3b_uniq UNIQUE (app_label, model);
 i   ALTER TABLE ONLY public.django_content_type DROP CONSTRAINT django_content_type_app_label_76bd3d3b_uniq;
       public         suncat_db_admin    false    173    173    173            �           2606    110017    django_content_type_pkey 
   CONSTRAINT     c   ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);
 V   ALTER TABLE ONLY public.django_content_type DROP CONSTRAINT django_content_type_pkey;
       public         suncat_db_admin    false    173    173            �           2606    110009    django_migrations_pkey 
   CONSTRAINT     _   ALTER TABLE ONLY django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);
 R   ALTER TABLE ONLY public.django_migrations DROP CONSTRAINT django_migrations_pkey;
       public         suncat_db_admin    false    171    171                       2606    110211    django_session_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);
 L   ALTER TABLE ONLY public.django_session DROP CONSTRAINT django_session_pkey;
       public         suncat_db_admin    false    198    198                       2606    110156    global_config_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY global_config
    ADD CONSTRAINT global_config_pkey PRIMARY KEY (gconf_id);
 J   ALTER TABLE ONLY public.global_config DROP CONSTRAINT global_config_pkey;
       public         suncat_db_admin    false    189    189            .           2606    110281 "   holdings_normalisation_config_pkey 
   CONSTRAINT     }   ALTER TABLE ONLY holdings_normalisation_config
    ADD CONSTRAINT holdings_normalisation_config_pkey PRIMARY KEY (nconf_id);
 j   ALTER TABLE ONLY public.holdings_normalisation_config DROP CONSTRAINT holdings_normalisation_config_pkey;
       public         suncat_db_admin    false    206    206                       2606    110166     institution_config_inst_code_key 
   CONSTRAINT     l   ALTER TABLE ONLY institution_config
    ADD CONSTRAINT institution_config_inst_code_key UNIQUE (inst_code);
 ]   ALTER TABLE ONLY public.institution_config DROP CONSTRAINT institution_config_inst_code_key;
       public         suncat_db_admin    false    191    191            
           2606    110164    institution_config_pkey 
   CONSTRAINT     g   ALTER TABLE ONLY institution_config
    ADD CONSTRAINT institution_config_pkey PRIMARY KEY (iconf_id);
 T   ALTER TABLE ONLY public.institution_config DROP CONSTRAINT institution_config_pkey;
       public         suncat_db_admin    false    191    191                       2606    110174    institution_log_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY institution_log
    ADD CONSTRAINT institution_log_pkey PRIMARY KEY (logid);
 N   ALTER TABLE ONLY public.institution_log DROP CONSTRAINT institution_log_pkey;
       public         suncat_db_admin    false    193    193                       2606    110184    marc_field_marc_code_key 
   CONSTRAINT     \   ALTER TABLE ONLY marc_field
    ADD CONSTRAINT marc_field_marc_code_key UNIQUE (marc_code);
 M   ALTER TABLE ONLY public.marc_field DROP CONSTRAINT marc_field_marc_code_key;
       public         suncat_db_admin    false    195    195                       2606    110182    marc_field_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY marc_field
    ADD CONSTRAINT marc_field_pkey PRIMARY KEY (mf_id);
 D   ALTER TABLE ONLY public.marc_field DROP CONSTRAINT marc_field_pkey;
       public         suncat_db_admin    false    195    195                       2606    110192    marc_subfield_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY marc_subfield
    ADD CONSTRAINT marc_subfield_pkey PRIMARY KEY (sf_id);
 J   ALTER TABLE ONLY public.marc_subfield DROP CONSTRAINT marc_subfield_pkey;
       public         suncat_db_admin    false    197    197                       2606    110194    marc_subfield_subfield_code_key 
   CONSTRAINT     j   ALTER TABLE ONLY marc_subfield
    ADD CONSTRAINT marc_subfield_subfield_code_key UNIQUE (subfield_code);
 W   ALTER TABLE ONLY public.marc_subfield DROP CONSTRAINT marc_subfield_subfield_code_key;
       public         suncat_db_admin    false    197    197            6           2606    110289    normalisation_config_pkey 
   CONSTRAINT     k   ALTER TABLE ONLY normalisation_config
    ADD CONSTRAINT normalisation_config_pkey PRIMARY KEY (nconf_id);
 X   ALTER TABLE ONLY public.normalisation_config DROP CONSTRAINT normalisation_config_pkey;
       public         suncat_db_admin    false    208    208            ;           2606    110307 (   normalisation_functions_display_name_key 
   CONSTRAINT     |   ALTER TABLE ONLY normalisation_functions
    ADD CONSTRAINT normalisation_functions_display_name_key UNIQUE (display_name);
 j   ALTER TABLE ONLY public.normalisation_functions DROP CONSTRAINT normalisation_functions_display_name_key;
       public         suncat_db_admin    false    212    212            >           2606    110309 )   normalisation_functions_function_name_key 
   CONSTRAINT     ~   ALTER TABLE ONLY normalisation_functions
    ADD CONSTRAINT normalisation_functions_function_name_key UNIQUE (function_name);
 k   ALTER TABLE ONLY public.normalisation_functions DROP CONSTRAINT normalisation_functions_function_name_key;
       public         suncat_db_admin    false    212    212            @           2606    110305    normalisation_functions_pkey 
   CONSTRAINT     p   ALTER TABLE ONLY normalisation_functions
    ADD CONSTRAINT normalisation_functions_pkey PRIMARY KEY (func_id);
 ^   ALTER TABLE ONLY public.normalisation_functions DROP CONSTRAINT normalisation_functions_pkey;
       public         suncat_db_admin    false    212    212            8           2606    110297    normalisation_group_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY normalisation_group
    ADD CONSTRAINT normalisation_group_pkey PRIMARY KEY (func_id);
 V   ALTER TABLE ONLY public.normalisation_group DROP CONSTRAINT normalisation_group_pkey;
       public         suncat_db_admin    false    210    210            "           2606    110221    skip_config_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY skip_config
    ADD CONSTRAINT skip_config_pkey PRIMARY KEY (skipconf_id);
 F   ALTER TABLE ONLY public.skip_config DROP CONSTRAINT skip_config_pkey;
       public         suncat_db_admin    false    200    200            $           2606    110229    skip_functions_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY skip_functions
    ADD CONSTRAINT skip_functions_pkey PRIMARY KEY (func_id);
 L   ALTER TABLE ONLY public.skip_functions DROP CONSTRAINT skip_functions_pkey;
       public         suncat_db_admin    false    202    202            &           2606    110237    skip_group_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY skip_group
    ADD CONSTRAINT skip_group_pkey PRIMARY KEY (func_id);
 D   ALTER TABLE ONLY public.skip_group DROP CONSTRAINT skip_group_pkey;
       public         suncat_db_admin    false    204    204            �           1259    110080    auth_group_name_a6ea08ec_like    INDEX     a   CREATE INDEX auth_group_name_a6ea08ec_like ON auth_group USING btree (name varchar_pattern_ops);
 1   DROP INDEX public.auth_group_name_a6ea08ec_like;
       public         suncat_db_admin    false    177            �           1259    110093    auth_group_permissions_0e939a4f    INDEX     _   CREATE INDEX auth_group_permissions_0e939a4f ON auth_group_permissions USING btree (group_id);
 3   DROP INDEX public.auth_group_permissions_0e939a4f;
       public         suncat_db_admin    false    179            �           1259    110094    auth_group_permissions_8373b171    INDEX     d   CREATE INDEX auth_group_permissions_8373b171 ON auth_group_permissions USING btree (permission_id);
 3   DROP INDEX public.auth_group_permissions_8373b171;
       public         suncat_db_admin    false    179            �           1259    110079    auth_permission_417f1b1c    INDEX     X   CREATE INDEX auth_permission_417f1b1c ON auth_permission USING btree (content_type_id);
 ,   DROP INDEX public.auth_permission_417f1b1c;
       public         suncat_db_admin    false    175            �           1259    110109    auth_user_groups_0e939a4f    INDEX     S   CREATE INDEX auth_user_groups_0e939a4f ON auth_user_groups USING btree (group_id);
 -   DROP INDEX public.auth_user_groups_0e939a4f;
       public         suncat_db_admin    false    183            �           1259    110108    auth_user_groups_e8701ad4    INDEX     R   CREATE INDEX auth_user_groups_e8701ad4 ON auth_user_groups USING btree (user_id);
 -   DROP INDEX public.auth_user_groups_e8701ad4;
       public         suncat_db_admin    false    183            �           1259    110123 #   auth_user_user_permissions_8373b171    INDEX     l   CREATE INDEX auth_user_user_permissions_8373b171 ON auth_user_user_permissions USING btree (permission_id);
 7   DROP INDEX public.auth_user_user_permissions_8373b171;
       public         suncat_db_admin    false    185            �           1259    110122 #   auth_user_user_permissions_e8701ad4    INDEX     f   CREATE INDEX auth_user_user_permissions_e8701ad4 ON auth_user_user_permissions USING btree (user_id);
 7   DROP INDEX public.auth_user_user_permissions_e8701ad4;
       public         suncat_db_admin    false    185            �           1259    110095     auth_user_username_6821ab7c_like    INDEX     g   CREATE INDEX auth_user_username_6821ab7c_like ON auth_user USING btree (username varchar_pattern_ops);
 4   DROP INDEX public.auth_user_username_6821ab7c_like;
       public         suncat_db_admin    false    181            �           1259    110146    django_admin_log_417f1b1c    INDEX     Z   CREATE INDEX django_admin_log_417f1b1c ON django_admin_log USING btree (content_type_id);
 -   DROP INDEX public.django_admin_log_417f1b1c;
       public         suncat_db_admin    false    187                        1259    110147    django_admin_log_e8701ad4    INDEX     R   CREATE INDEX django_admin_log_e8701ad4 ON django_admin_log USING btree (user_id);
 -   DROP INDEX public.django_admin_log_e8701ad4;
       public         suncat_db_admin    false    187                       1259    110212    django_session_de54fa62    INDEX     R   CREATE INDEX django_session_de54fa62 ON django_session USING btree (expire_date);
 +   DROP INDEX public.django_session_de54fa62;
       public         suncat_db_admin    false    198                       1259    110213 (   django_session_session_key_c0390e0f_like    INDEX     w   CREATE INDEX django_session_session_key_c0390e0f_like ON django_session USING btree (session_key varchar_pattern_ops);
 <   DROP INDEX public.django_session_session_key_c0390e0f_like;
       public         suncat_db_admin    false    198            '           1259    110366 &   holdings_normalisation_config_681f4cab    INDEX     q   CREATE INDEX holdings_normalisation_config_681f4cab ON holdings_normalisation_config USING btree (marcfield_id);
 :   DROP INDEX public.holdings_normalisation_config_681f4cab;
       public         suncat_db_admin    false    206            (           1259    110372 &   holdings_normalisation_config_75a73542    INDEX     r   CREATE INDEX holdings_normalisation_config_75a73542 ON holdings_normalisation_config USING btree (subfield_1_id);
 :   DROP INDEX public.holdings_normalisation_config_75a73542;
       public         suncat_db_admin    false    206            )           1259    110354 &   holdings_normalisation_config_d327c2ad    INDEX     v   CREATE INDEX holdings_normalisation_config_d327c2ad ON holdings_normalisation_config USING btree (group_sequence_id);
 :   DROP INDEX public.holdings_normalisation_config_d327c2ad;
       public         suncat_db_admin    false    206            *           1259    110348 &   holdings_normalisation_config_d60961ca    INDEX     p   CREATE INDEX holdings_normalisation_config_d60961ca ON holdings_normalisation_config USING btree (function_id);
 :   DROP INDEX public.holdings_normalisation_config_d60961ca;
       public         suncat_db_admin    false    206            +           1259    110360 &   holdings_normalisation_config_de36e083    INDEX     q   CREATE INDEX holdings_normalisation_config_de36e083 ON holdings_normalisation_config USING btree (inst_conf_id);
 :   DROP INDEX public.holdings_normalisation_config_de36e083;
       public         suncat_db_admin    false    206            ,           1259    110378 &   holdings_normalisation_config_ea18fe13    INDEX     r   CREATE INDEX holdings_normalisation_config_ea18fe13 ON holdings_normalisation_config USING btree (subfield_2_id);
 :   DROP INDEX public.holdings_normalisation_config_ea18fe13;
       public         suncat_db_admin    false    206                       1259    110200    institution_config_5c23a009    INDEX     _   CREATE INDEX institution_config_5c23a009 ON institution_config USING btree (global_config_id);
 /   DROP INDEX public.institution_config_5c23a009;
       public         suncat_db_admin    false    191                       1259    110201 *   institution_config_inst_code_4846eadf_like    INDEX     {   CREATE INDEX institution_config_inst_code_4846eadf_like ON institution_config USING btree (inst_code varchar_pattern_ops);
 >   DROP INDEX public.institution_config_inst_code_4846eadf_like;
       public         suncat_db_admin    false    191                       1259    110202 "   marc_field_marc_code_cc0cb3a0_like    INDEX     k   CREATE INDEX marc_field_marc_code_cc0cb3a0_like ON marc_field USING btree (marc_code varchar_pattern_ops);
 6   DROP INDEX public.marc_field_marc_code_cc0cb3a0_like;
       public         suncat_db_admin    false    195                       1259    110203 )   marc_subfield_subfield_code_08fbc19b_like    INDEX     y   CREATE INDEX marc_subfield_subfield_code_08fbc19b_like ON marc_subfield USING btree (subfield_code varchar_pattern_ops);
 =   DROP INDEX public.marc_subfield_subfield_code_08fbc19b_like;
       public         suncat_db_admin    false    197            /           1259    110330    normalisation_config_681f4cab    INDEX     _   CREATE INDEX normalisation_config_681f4cab ON normalisation_config USING btree (marcfield_id);
 1   DROP INDEX public.normalisation_config_681f4cab;
       public         suncat_db_admin    false    208            0           1259    110336    normalisation_config_75a73542    INDEX     `   CREATE INDEX normalisation_config_75a73542 ON normalisation_config USING btree (subfield_1_id);
 1   DROP INDEX public.normalisation_config_75a73542;
       public         suncat_db_admin    false    208            1           1259    110318    normalisation_config_d327c2ad    INDEX     d   CREATE INDEX normalisation_config_d327c2ad ON normalisation_config USING btree (group_sequence_id);
 1   DROP INDEX public.normalisation_config_d327c2ad;
       public         suncat_db_admin    false    208            2           1259    110312    normalisation_config_d60961ca    INDEX     ^   CREATE INDEX normalisation_config_d60961ca ON normalisation_config USING btree (function_id);
 1   DROP INDEX public.normalisation_config_d60961ca;
       public         suncat_db_admin    false    208            3           1259    110324    normalisation_config_de36e083    INDEX     _   CREATE INDEX normalisation_config_de36e083 ON normalisation_config USING btree (inst_conf_id);
 1   DROP INDEX public.normalisation_config_de36e083;
       public         suncat_db_admin    false    208            4           1259    110342    normalisation_config_ea18fe13    INDEX     `   CREATE INDEX normalisation_config_ea18fe13 ON normalisation_config USING btree (subfield_2_id);
 1   DROP INDEX public.normalisation_config_ea18fe13;
       public         suncat_db_admin    false    208            9           1259    110310 2   normalisation_functions_display_name_cb095acd_like    INDEX     �   CREATE INDEX normalisation_functions_display_name_cb095acd_like ON normalisation_functions USING btree (display_name varchar_pattern_ops);
 F   DROP INDEX public.normalisation_functions_display_name_cb095acd_like;
       public         suncat_db_admin    false    212            <           1259    110311 3   normalisation_functions_function_name_ac56bd08_like    INDEX     �   CREATE INDEX normalisation_functions_function_name_ac56bd08_like ON normalisation_functions USING btree (function_name varchar_pattern_ops);
 G   DROP INDEX public.normalisation_functions_function_name_ac56bd08_like;
       public         suncat_db_admin    false    212                       1259    110243    skip_config_021d1672    INDEX     W   CREATE INDEX skip_config_021d1672 ON skip_config USING btree (condition_marcfield_id);
 (   DROP INDEX public.skip_config_021d1672;
       public         suncat_db_admin    false    200                       1259    110250    skip_config_5eba65b4    INDEX     N   CREATE INDEX skip_config_5eba65b4 ON skip_config USING btree (skip_group_id);
 (   DROP INDEX public.skip_config_5eba65b4;
       public         suncat_db_admin    false    200                       1259    110244    skip_config_88c1c2a6    INDEX     Q   CREATE INDEX skip_config_88c1c2a6 ON skip_config USING btree (skip_function_id);
 (   DROP INDEX public.skip_config_88c1c2a6;
       public         suncat_db_admin    false    200                       1259    110256    skip_config_8a4d9ef2    INDEX     R   CREATE INDEX skip_config_8a4d9ef2 ON skip_config USING btree (skip_inst_conf_id);
 (   DROP INDEX public.skip_config_8a4d9ef2;
       public         suncat_db_admin    false    200                       1259    110262    skip_config_c2be69ee    INDEX     R   CREATE INDEX skip_config_c2be69ee ON skip_config USING btree (skip_marcfield_id);
 (   DROP INDEX public.skip_config_c2be69ee;
       public         suncat_db_admin    false    200                        1259    110268    skip_config_fe576a19    INDEX     S   CREATE INDEX skip_config_fe576a19 ON skip_config USING btree (skip_subfield_1_id);
 (   DROP INDEX public.skip_config_fe576a19;
       public         suncat_db_admin    false    200            C           2606    110086 ?   auth_group_permiss_permission_id_84c5c92e_fk_auth_permission_id    FK CONSTRAINT     �   ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permiss_permission_id_84c5c92e_fk_auth_permission_id FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;
 �   ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permiss_permission_id_84c5c92e_fk_auth_permission_id;
       public       suncat_db_admin    false    175    179    2018            B           2606    110081 9   auth_group_permissions_group_id_b120cbf9_fk_auth_group_id    FK CONSTRAINT     �   ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;
 z   ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id;
       public       suncat_db_admin    false    177    2023    179            A           2606    110072 ?   auth_permiss_content_type_id_2f476e4b_fk_django_content_type_id    FK CONSTRAINT     �   ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permiss_content_type_id_2f476e4b_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;
 y   ALTER TABLE ONLY public.auth_permission DROP CONSTRAINT auth_permiss_content_type_id_2f476e4b_fk_django_content_type_id;
       public       suncat_db_admin    false    173    2013    175            E           2606    110101 3   auth_user_groups_group_id_97559544_fk_auth_group_id    FK CONSTRAINT     �   ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;
 n   ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id;
       public       suncat_db_admin    false    2023    177    183            D           2606    110096 1   auth_user_groups_user_id_6a12ed8b_fk_auth_user_id    FK CONSTRAINT     �   ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 l   ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id;
       public       suncat_db_admin    false    181    183    2031            G           2606    110115 ?   auth_user_user_per_permission_id_1fbb5f2c_fk_auth_permission_id    FK CONSTRAINT     �   ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_per_permission_id_1fbb5f2c_fk_auth_permission_id FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;
 �   ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_per_permission_id_1fbb5f2c_fk_auth_permission_id;
       public       suncat_db_admin    false    185    2018    175            F           2606    110110 ;   auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id    FK CONSTRAINT     �   ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 �   ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id;
       public       suncat_db_admin    false    185    2031    181            H           2606    110136 ?   django_admin_content_type_id_c4bce8eb_fk_django_content_type_id    FK CONSTRAINT     �   ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_content_type_id_c4bce8eb_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;
 z   ALTER TABLE ONLY public.django_admin_log DROP CONSTRAINT django_admin_content_type_id_c4bce8eb_fk_django_content_type_id;
       public       suncat_db_admin    false    2013    173    187            I           2606    110141 1   django_admin_log_user_id_c564eba6_fk_auth_user_id    FK CONSTRAINT     �   ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 l   ALTER TABLE ONLY public.django_admin_log DROP CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id;
       public       suncat_db_admin    false    181    187    2031            R           2606    110355 ?   holdi_group_sequence_id_18290465_fk_normalisation_group_func_id    FK CONSTRAINT     �   ALTER TABLE ONLY holdings_normalisation_config
    ADD CONSTRAINT holdi_group_sequence_id_18290465_fk_normalisation_group_func_id FOREIGN KEY (group_sequence_id) REFERENCES normalisation_group(func_id) DEFERRABLE INITIALLY DEFERRED;
 �   ALTER TABLE ONLY public.holdings_normalisation_config DROP CONSTRAINT holdi_group_sequence_id_18290465_fk_normalisation_group_func_id;
       public       suncat_db_admin    false    2104    206    210            Q           2606    110349 ?   holding_function_id_d494bd74_fk_normalisation_functions_func_id    FK CONSTRAINT     �   ALTER TABLE ONLY holdings_normalisation_config
    ADD CONSTRAINT holding_function_id_d494bd74_fk_normalisation_functions_func_id FOREIGN KEY (function_id) REFERENCES normalisation_functions(func_id) DEFERRABLE INITIALLY DEFERRED;
 �   ALTER TABLE ONLY public.holdings_normalisation_config DROP CONSTRAINT holding_function_id_d494bd74_fk_normalisation_functions_func_id;
       public       suncat_db_admin    false    206    212    2112            S           2606    110361 ?   holdings_n_inst_conf_id_f8c80f2f_fk_institution_config_iconf_id    FK CONSTRAINT     �   ALTER TABLE ONLY holdings_normalisation_config
    ADD CONSTRAINT holdings_n_inst_conf_id_f8c80f2f_fk_institution_config_iconf_id FOREIGN KEY (inst_conf_id) REFERENCES institution_config(iconf_id) DEFERRABLE INITIALLY DEFERRED;
 �   ALTER TABLE ONLY public.holdings_normalisation_config DROP CONSTRAINT holdings_n_inst_conf_id_f8c80f2f_fk_institution_config_iconf_id;
       public       suncat_db_admin    false    2058    206    191            U           2606    110373 ?   holdings_normalis_subfield_1_id_4f8e184f_fk_marc_subfield_sf_id    FK CONSTRAINT     �   ALTER TABLE ONLY holdings_normalisation_config
    ADD CONSTRAINT holdings_normalis_subfield_1_id_4f8e184f_fk_marc_subfield_sf_id FOREIGN KEY (subfield_1_id) REFERENCES marc_subfield(sf_id) DEFERRABLE INITIALLY DEFERRED;
 �   ALTER TABLE ONLY public.holdings_normalisation_config DROP CONSTRAINT holdings_normalis_subfield_1_id_4f8e184f_fk_marc_subfield_sf_id;
       public       suncat_db_admin    false    2067    197    206            V           2606    110379 ?   holdings_normalis_subfield_2_id_9b92a8b2_fk_marc_subfield_sf_id    FK CONSTRAINT     �   ALTER TABLE ONLY holdings_normalisation_config
    ADD CONSTRAINT holdings_normalis_subfield_2_id_9b92a8b2_fk_marc_subfield_sf_id FOREIGN KEY (subfield_2_id) REFERENCES marc_subfield(sf_id) DEFERRABLE INITIALLY DEFERRED;
 �   ALTER TABLE ONLY public.holdings_normalisation_config DROP CONSTRAINT holdings_normalis_subfield_2_id_9b92a8b2_fk_marc_subfield_sf_id;
       public       suncat_db_admin    false    206    2067    197            T           2606    110367 ?   holdings_normalisatio_marcfield_id_2115dc29_fk_marc_field_mf_id    FK CONSTRAINT     �   ALTER TABLE ONLY holdings_normalisation_config
    ADD CONSTRAINT holdings_normalisatio_marcfield_id_2115dc29_fk_marc_field_mf_id FOREIGN KEY (marcfield_id) REFERENCES marc_field(mf_id) DEFERRABLE INITIALLY DEFERRED;
 �   ALTER TABLE ONLY public.holdings_normalisation_config DROP CONSTRAINT holdings_normalisatio_marcfield_id_2115dc29_fk_marc_field_mf_id;
       public       suncat_db_admin    false    2065    206    195            J           2606    110195 ?   institution_global_config_id_4ab73af2_fk_global_config_gconf_id    FK CONSTRAINT     �   ALTER TABLE ONLY institution_config
    ADD CONSTRAINT institution_global_config_id_4ab73af2_fk_global_config_gconf_id FOREIGN KEY (global_config_id) REFERENCES global_config(gconf_id) DEFERRABLE INITIALLY DEFERRED;
 |   ALTER TABLE ONLY public.institution_config DROP CONSTRAINT institution_global_config_id_4ab73af2_fk_global_config_gconf_id;
       public       suncat_db_admin    false    2052    191    189            X           2606    110319 ?   norma_group_sequence_id_49aabb65_fk_normalisation_group_func_id    FK CONSTRAINT     �   ALTER TABLE ONLY normalisation_config
    ADD CONSTRAINT norma_group_sequence_id_49aabb65_fk_normalisation_group_func_id FOREIGN KEY (group_sequence_id) REFERENCES normalisation_group(func_id) DEFERRABLE INITIALLY DEFERRED;
 ~   ALTER TABLE ONLY public.normalisation_config DROP CONSTRAINT norma_group_sequence_id_49aabb65_fk_normalisation_group_func_id;
       public       suncat_db_admin    false    208    210    2104            W           2606    110313 ?   normali_function_id_1da75493_fk_normalisation_functions_func_id    FK CONSTRAINT     �   ALTER TABLE ONLY normalisation_config
    ADD CONSTRAINT normali_function_id_1da75493_fk_normalisation_functions_func_id FOREIGN KEY (function_id) REFERENCES normalisation_functions(func_id) DEFERRABLE INITIALLY DEFERRED;
 ~   ALTER TABLE ONLY public.normalisation_config DROP CONSTRAINT normali_function_id_1da75493_fk_normalisation_functions_func_id;
       public       suncat_db_admin    false    212    2112    208            Y           2606    110325 ?   normalisat_inst_conf_id_27e5a024_fk_institution_config_iconf_id    FK CONSTRAINT     �   ALTER TABLE ONLY normalisation_config
    ADD CONSTRAINT normalisat_inst_conf_id_27e5a024_fk_institution_config_iconf_id FOREIGN KEY (inst_conf_id) REFERENCES institution_config(iconf_id) DEFERRABLE INITIALLY DEFERRED;
 ~   ALTER TABLE ONLY public.normalisation_config DROP CONSTRAINT normalisat_inst_conf_id_27e5a024_fk_institution_config_iconf_id;
       public       suncat_db_admin    false    191    208    2058            [           2606    110337 ?   normalisation_con_subfield_1_id_b3ac2230_fk_marc_subfield_sf_id    FK CONSTRAINT     �   ALTER TABLE ONLY normalisation_config
    ADD CONSTRAINT normalisation_con_subfield_1_id_b3ac2230_fk_marc_subfield_sf_id FOREIGN KEY (subfield_1_id) REFERENCES marc_subfield(sf_id) DEFERRABLE INITIALLY DEFERRED;
 ~   ALTER TABLE ONLY public.normalisation_config DROP CONSTRAINT normalisation_con_subfield_1_id_b3ac2230_fk_marc_subfield_sf_id;
       public       suncat_db_admin    false    197    208    2067            \           2606    110343 ?   normalisation_con_subfield_2_id_44941c47_fk_marc_subfield_sf_id    FK CONSTRAINT     �   ALTER TABLE ONLY normalisation_config
    ADD CONSTRAINT normalisation_con_subfield_2_id_44941c47_fk_marc_subfield_sf_id FOREIGN KEY (subfield_2_id) REFERENCES marc_subfield(sf_id) DEFERRABLE INITIALLY DEFERRED;
 ~   ALTER TABLE ONLY public.normalisation_config DROP CONSTRAINT normalisation_con_subfield_2_id_44941c47_fk_marc_subfield_sf_id;
       public       suncat_db_admin    false    197    208    2067            Z           2606    110331 >   normalisation_config_marcfield_id_f04889bd_fk_marc_field_mf_id    FK CONSTRAINT     �   ALTER TABLE ONLY normalisation_config
    ADD CONSTRAINT normalisation_config_marcfield_id_f04889bd_fk_marc_field_mf_id FOREIGN KEY (marcfield_id) REFERENCES marc_field(mf_id) DEFERRABLE INITIALLY DEFERRED;
 }   ALTER TABLE ONLY public.normalisation_config DROP CONSTRAINT normalisation_config_marcfield_id_f04889bd_fk_marc_field_mf_id;
       public       suncat_db_admin    false    208    2065    195            N           2606    110257 ?   skip__skip_inst_conf_id_072013dc_fk_institution_config_iconf_id    FK CONSTRAINT     �   ALTER TABLE ONLY skip_config
    ADD CONSTRAINT skip__skip_inst_conf_id_072013dc_fk_institution_config_iconf_id FOREIGN KEY (skip_inst_conf_id) REFERENCES institution_config(iconf_id) DEFERRABLE INITIALLY DEFERRED;
 u   ALTER TABLE ONLY public.skip_config DROP CONSTRAINT skip__skip_inst_conf_id_072013dc_fk_institution_config_iconf_id;
       public       suncat_db_admin    false    191    2058    200            K           2606    110238 ?   skip_config_condition_marcfield_id_f20c96a3_fk_marc_field_mf_id    FK CONSTRAINT     �   ALTER TABLE ONLY skip_config
    ADD CONSTRAINT skip_config_condition_marcfield_id_f20c96a3_fk_marc_field_mf_id FOREIGN KEY (condition_marcfield_id) REFERENCES marc_field(mf_id) DEFERRABLE INITIALLY DEFERRED;
 u   ALTER TABLE ONLY public.skip_config DROP CONSTRAINT skip_config_condition_marcfield_id_f20c96a3_fk_marc_field_mf_id;
       public       suncat_db_admin    false    200    2065    195            L           2606    110245 ?   skip_config_skip_function_id_d9b51ade_fk_skip_functions_func_id    FK CONSTRAINT     �   ALTER TABLE ONLY skip_config
    ADD CONSTRAINT skip_config_skip_function_id_d9b51ade_fk_skip_functions_func_id FOREIGN KEY (skip_function_id) REFERENCES skip_functions(func_id) DEFERRABLE INITIALLY DEFERRED;
 u   ALTER TABLE ONLY public.skip_config DROP CONSTRAINT skip_config_skip_function_id_d9b51ade_fk_skip_functions_func_id;
       public       suncat_db_admin    false    2084    202    200            M           2606    110251 8   skip_config_skip_group_id_e957b9c7_fk_skip_group_func_id    FK CONSTRAINT     �   ALTER TABLE ONLY skip_config
    ADD CONSTRAINT skip_config_skip_group_id_e957b9c7_fk_skip_group_func_id FOREIGN KEY (skip_group_id) REFERENCES skip_group(func_id) DEFERRABLE INITIALLY DEFERRED;
 n   ALTER TABLE ONLY public.skip_config DROP CONSTRAINT skip_config_skip_group_id_e957b9c7_fk_skip_group_func_id;
       public       suncat_db_admin    false    200    2086    204            O           2606    110263 :   skip_config_skip_marcfield_id_96a4d633_fk_marc_field_mf_id    FK CONSTRAINT     �   ALTER TABLE ONLY skip_config
    ADD CONSTRAINT skip_config_skip_marcfield_id_96a4d633_fk_marc_field_mf_id FOREIGN KEY (skip_marcfield_id) REFERENCES marc_field(mf_id) DEFERRABLE INITIALLY DEFERRED;
 p   ALTER TABLE ONLY public.skip_config DROP CONSTRAINT skip_config_skip_marcfield_id_96a4d633_fk_marc_field_mf_id;
       public       suncat_db_admin    false    195    200    2065            P           2606    110269 >   skip_config_skip_subfield_1_id_0bc9f2e0_fk_marc_subfield_sf_id    FK CONSTRAINT     �   ALTER TABLE ONLY skip_config
    ADD CONSTRAINT skip_config_skip_subfield_1_id_0bc9f2e0_fk_marc_subfield_sf_id FOREIGN KEY (skip_subfield_1_id) REFERENCES marc_subfield(sf_id) DEFERRABLE INITIALLY DEFERRED;
 t   ALTER TABLE ONLY public.skip_config DROP CONSTRAINT skip_config_skip_subfield_1_id_0bc9f2e0_fk_marc_subfield_sf_id;
       public       suncat_db_admin    false    2067    200    197            �      x������ � �      �      x������ � �      �   q  x����n�0E��W�a^ݽ���H3�diD7AC�c���e�
�H�5�{˧}-�c������M=��3���_���m�rj:U�<���G�jI���I�����6S��d��/FO���?X�oP�9f;�!N�,0��:���ک[Sσ~bix�F�����{].���.{G]D��ٵU��1��C��a�'� ��^Ŷ���}�%Lz�5��~�+L~�5u�?��{�F�z��z�Z׬6�sm� O9f�: �:
����S#ۊ�n�3'LB����g�@� ����������P�I0 9��� �����R��L�~�gM��
^�b$� �S���	�vL�����j�ES`#)���2�Z+ϻ���ǥi}9���"xq�tl)�XP �B8�ixk`="������-E���8.xwiP�f]E ��u�{by}�T��Ʉ���~�)��8��$>���N��T��po�QS�������R��I
S5`�O��|R��'y�6H��$����4���(&L\��yF��.�FA��*`�𩸼�i9�h��%&���xNBq��m��d�� ��#q�p�&��$��~�VG���g������$      �   �   x�3�,H�NI3�/�H425S12100P1����,(p��T���J*�+)�7*�O������NͪL�L�J��qs)u+w��4204�50�52S0��26�24�30032��60�,�,N�/)���`R"�0݆�
��V�V&@�F&&& �\1z\\\ ;0�      �      x������ � �      �      x������ � �      �   �  x�ŘKo�F��ԧX����bg߻��q\�@�s襀A�+��D*|$ͷ�#iE�R#ۀ���ffg�?���7D�P��[&-��p	R�B ��m���Y:w�~KS��?�E���
�Jw��2^dU\gE��"�e�\��zt�E�8�5^4e9���,s�Y���`B�c��0��?d����c��+�4e���.)������͛_�ܳ]�@-+.8���o��|����<���5�F����O��<ݼ�hV�O���Dc��@�!�[N[ΕK2��=�	Pg���M��&'>����.Y���0{���a�#G��O��6��R�>��3�f�0�?YUW]QV����[���>~b�&~RiK�D���2.����r�T�E���)�#\�hVO��Ҹܗ����!�XRn�IG���L�����$�8جɓ��b��{h@6�G��Za���Ϡ�!���PM���h����&G*�*h,@�Nc��3F��pw�p<��]I�602m�pm��Ӱg��{���A�Ag��Rj>���H�q��>�/	�sU ��1,�(= y^�;t�f�`XI��w�	�X׺�06$_���p\[�0Nٛ��^��"��%a0��Q�?
������]�^�{}�r���ٕ߲ʡ�?����}�_��h D��,�PRh��
���Sl�����?�Ebۇ�T
AF��z{��C4�-�8��^�A�!Z�?XjjLw>����8���W�ې�x> ��x��;U�yى8%�^�ݻ��FTf���Sڵ��>����??>Z��mB��n��?�&^����e�wՏ>���+n��T����F+���e�ɑu�zr�W��ѧ��*$\atA{�
��=�� a��/+��_����I���W���Yy�R@��9��������q���B��H�%�`�A��9;i����o<�L�����      �   �   x�u�K�!D��aF���.#�H7M��A���/��]�eSZ�3$�`<�)��XnZ6�rB�}���pw���{�)��݊���������x6~%���������jk=/��-\j��G��z�4�I,m���Ga�<��m��ƕVJ�!���r�ds`�9�_���a��ڌ4u�2F�-Ym���7�]��"�����      �   g  x���Mn� ���)��b��59K%�b�������H�F�[w����@s��dƔ���B�m��5? v x8RvĤ��>#hq��������3Y�у��D���~K�r�9�*?]���OJ�6���0����p�PeG�ڛm��_-c�]2A�&x���*W^(g�s>�I���:�XQ�Zb^����Ma
Q(�J�����q�9�m����\��tL%p��-n�������s��k�5uU2ۆ�p�>�"�\���NS��geB���&F}6��%�5a��w�s3z����xn�A�&����`���2��7;��6�a���S k�2�t��,Uo/]۶�8�X      �   g  x����n�0 ����/���JIv��:��H�f7 *Q��Mv�W09�IN�_�}����'� �^Ot
Bմ`ߔwU��M�9����!G^�C̜w�֓l9Gl)r��r�~�H?e��ZM��+�W"��r�5�2O��7r�$�g�;��-Sq��c�����V��	E����jfx��pA��]�섣k�u�v�E#��pD��ˮxJ��W��S���ү�r���o�̪���}�Z�>��� #D�d2�}�w �P�A{��iX�{�.YU1n�r���>�!Ć��i�N $O;<�Oe�@��%Eʘ4��[_���/��j h mL1��>�~ƃ����7�      �      x�3�I-.1����� ��      �   1   x�3�L�4���������<μ�g(�� (g�il�idp��qqq 5�      �   �   x�m��
�0E盯��)����A�v��>jЦ�hſ7����r���>ޓ�ّ}���e=�&��"�) )f0O�2�T&�t�����'�040j��H�����m�(Q���Zm~ʢ�@_�����m�1�Z�C���>�4)      �      x������ � �      �   �   x���RE1�u�0L��]���0�u(�_���v��4�����a`�3�l�tc��}~�������?�?�FA[�,��%7G��.��I�IlZc��6�Hr�� ֢/D4݄Mw��S�
�-(�F݅���O���5
n�0���w�N
�PӥB(`lI�q-��s)�1���2l���rI�2��>E{GH���[�,*�0�?ǄB�#e��Z�`m�&��(|/���^H�(�A�      �   N   x��I
�@ E��{����ҹ�#B@\	N��*\,\��lv>^�t'7�f���4�j*�4ަ��`7+_3f������g�      �   B  x��U�n�0<��§�"�ˇ�S�{�K�I
�
��ql�v ���w�)Yr��I�h9;\9X����o7/�׃��˕���a�Y/����7���	��
(��nF��ɷ�7� F�#�B�GWĿ��r�lW{�:
L�EG�H�MT��/aE\�,���nU�\b�� �c��2�縀����$@q��߿��:��cP��>C`ƒuU0��1M��T�P��j�ͻo"W��yY(��PD�O�]8ESP����e�6S-=#-������Ҝ�s˂�yo�ʪ���=ԡ�*�9��)����_��O��FH��%�_ɋ)]7]N�.����g�����t"�i��o]�{���l� }fH�<$(�^Z���s���!������H�lsS�����D�/x��q:>������Z��K^��F�b��Ѵ5�FdJ4mυf�b�/6��c����24"����ux�4<;Mƶ�7��*�B-�n�;�������ֿ���!��{GV�������z|�+X'Wv��mm��o���3���w'��Nҫ��(�6�x�nrg�������f� ��      �   z  x�uV�n�0=�_�[[�(�X��S��	�C��z*@��E�����;�M��lH�<�Y�bEn^�������{m�S+"��;�o��Т �R9a��֑���L��������ĄWn�+�=w�-*�Pl�(��a]}ȓp�[�\��
��YM�y�i:f��^
�2`�^j5
b��'�Zl��|���rA�{82������K��_�9�-b�(�Ŵ,�Y�L�	iM�gq���]�O�+�{|�c��,y9Gs��yZ�y�{WeZV�J(2Za��Xt�rC~t�y�S©	�4(G� ���%yx�,�m��Gv90r }D�GA׋�u�C6X����wW���Vt�$?�N�[����)��_�������r�BZ�9��)I��x�����[Ƥ��|퉴%$��d���hyu-"��=x;�
8f}E�-kFc JfD�Mi����/��{'w<����ÝV-L�=��,����B̸"2o�}�s/4`6Qv����+r-w��1Hֽ?��Cz�ZZ-潉)�}t$�V����V˹��[�{�Ks�㨃?�F����rrP"l����UX\t��5����b�g!N���3��t��:l��2��y^'�L�<ǫ��|aXO�W��Vk�K���e��mN��s/,@��O�"�D~[�n#�0w<Z\͘C�Ҏb��ds�'�ڜ{c'��\B��m��J�O�cz�7n\�F�G�QS̹���I�c�C� �X�w� �w�uP�x1���6<�p�Ok����څj��Q�V��V�Z4�	��M�S�G��fIn;(�D#Aۦi����F���[:ܟ��P�K����c!l��;B����*};�����4ݭ�6�C�QJ�#X9�      �   *   x���  ��w;�w�ø�&�m&��X��k��[�V5�      �   b  x�m��j�0D�Χ�P"۲����-,
���j��fk59��-͌�ޯ��s�����$M�J�-�t��}����_�=e���-�_:KQW�p�6�:���B���:-+�`lF���j�d���l�-�����L%�+3�F6V��xf�0���dJ&��dJ�ea"�a���L�fJ
V��SX�z�k�퀯Z���w0��hUw����2a�:)��A �ʶ A��h!�(c�z�O��9����`52��O�RO��p�,?�b\9hT�.+��w�d��+�D��XP(�QZ1�L�}*��%l������gb`����`Fexz� ��=fg���m۾~���      �     x�}�AS� ���W�^�P�T����Wg��)BD'�^Z�����{�ݷ,{����<_����.s�p�y �H�]Rը���/и$=b>`�J���W`�hTe	�ߺ+P_��'��g<BR��,J,A�o�/���G�ِ�f��!�F��c��Q��,,<���d˳� �����Gt.kM(J�Gk��?��6[U��A_W���j����(CǷ=��.:���)l���븶�]M'p?�����k��?c��ޯ	!��ס      �   O   x�˹1�@{��X��\�q��.T����`fbde��99�\l��#�ba�G8��V\����x��F�z��|?���     