To run this locally, just run:

	python manage.py migrate (after setting up the db in settings.py)
	python manage.py createsuperuser (to create your admin user)
	python manage.py runserver
